/* 
 * Angular JS
 * 
 * 
 */
angular.module('angular-validator-demo', ['angularValidator']);

angular.module('angular-validator-demo').controller('DemoCtrl', function ($scope) {
    $scope.submitMyForm = function () {
        addAccount();      
    };
    $scope.myCustomValidator = function (text) {
        return true;
    };
    $scope.anotherCustomValidator = function (text) {
        if (text === "rainbow") {
            return true;
        }
        else
            return "type in 'rainbow'";
    };
    $scope.passwordValidator = function (password) {

        if (!password) {
            return;
        }

        if (password.length < 6) {
            return "Password must be at least " + 6 + " characters long";
        }

        if (!password.match(/[A-Z]/)) {
            return "Password must have at least one capital letter";
        }

        if (!password.match(/[0-9]/)) {
            return "Password must have at least one number";
        }
        if (password.length > 50) {
            return "Password must be maximum 50 characters long";
        }

        return true;
    };
    $scope.userNameCustomValidator = function (username) {

        if (!username) {
            return;
        }

        if (username.length < 6) {
            return "User name must be at least 6 characters long";
        }
        if (username.length > 50) {
            return "Password must be maximum 50 characters long";
        }
    };
});


// jQuery
function showSignUp()
{
    $('#loginbox').hide();
    $('#signupbox').show();
}


/*
 * Check login
 * @returns {undefined}
 * 
 */
function checklogin() {
    var arrVal = {
        username: $("#login-username").val(),
        password: $("#login-password").val()
    };
    var string_directory = "http://projecttracking.com/www/ajax/userManagement/checkLogin.php";
    $.ajax({
        type: "POST",
        url: string_directory,
        dataType: "json",
        data: {userName: arrVal.username, passWord: arrVal.password},
        cache: false,
        ContentType: "application/json",
        success: function (result) {
            if (result === "true") {
                loadDashBoard(arrVal);
            }
            else if (result === "false") {
                document.getElementById("warning-error-login").innerHTML = "PLease, check your username, password correct!";
            }
            else {
                document.getElementById("warning-error-login").innerHTML = result;
            }
        },
        error: function (request, error) {
            alert('Network error has occurred please try again!');
        }

    });
}
function getUserLogin() {
    if (isSetCookie("loginUser"))
    {
        alert(getCookie());
    }
}


function setCookie(cname, cvalue) {
    var d = new Date();
    d.setTime(d.getTime() + (30 * 86400 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function isSetCookie(cname) {
    var vote = getCookie(cname);
    if (vote !== "") {
        //da set
        return true;
    } else {
        //chua set
        return false;
    }
}


//function to add new Account
function addAccount() {
    var arrVal = {
        username: $('#signup-userName').val(),
        password: $('#signup-password').val(),
        fullName: $('#fullName').val(),
        email: $('#emailAddress').val(),
        userId: 0
        //joinDate: new Date(),
       // avatarUrl: "",
       // userTypeId: 1
    };
    console.log(arrVal);

    $.ajax({
        type: "POST",
        //url: "../../library/addNewAccount.php",
        url: "http://projecttracking.com/www/ajax/userManagement/addNewAccount.php",
        data: {userName: arrVal.username, passWord: arrVal.password, fullName: arrVal.fullName, email: arrVal.email
            },
        cache: false,
        success: function (result) {
            if (result !== "error" && result !== "This account already exits")
            {
                console.log(result);
                arrVal.userId = result;
                loadDashBoard(arrVal);
            }
            else {
              $("#warning-error").val(result);
              $("#warning-error").text(result);
            }

        },
        error: function (request, error) {
            alert('Network error has occurred please try again!');
        }
    });
}


/// DASH BOARD functionality
function  loadDashBoard(arrVal){
    window.location.replace("partials/dashboard.html");
    window.location.href = "partials/dashboard.html";    
    // Set value and text for userName
    
    console.log("User name + " + arrVal.username);
    $('#userName').text(arrVal.username);
    $('#userName').val(arrVal.username);
    setCookie("loginUser", arrVal.username);
}


function SignOut(){
    
}