app.directive('formElement', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            label: "@",
            model: "="
        },
        link: function (scope, element, attrs) {
            scope.disabled = attrs.hasOwnProperty('disabled');
            scope.required = attrs.hasOwnProperty('required');
            scope.pattern = attrs.pattern || '.*';
        },
        template: '<div class="form-group"><label class="col-sm-3 control-label no-padding-right" >  {{label}}</label><div class="col-sm-7"><span class="block input-icon input-icon-right" ng-transclude></span></div></div>'
    };

})
        .directive('onlyNumbers', function () {
            return function (scope, element, attrs) {
                var keyCode = [8, 9, 13, 37, 39, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110, 190];
                element.bind("keydown", function (event) {
                    if ($.inArray(event.which, keyCode) === -1) {
                        scope.$apply(function () {
                            scope.$eval(attrs.onlyNum);
                            event.preventDefault();
                        });
                        event.preventDefault();
                    }

                });
            };
        })
        .directive('focusing', function () {
            return function (scope, element) {
                element[0].focus();
            };
        })
        .directive('animateOnChange', function ($animate) {
            return function (scope, elem, attr) {
                scope.$watch(attr.animateOnChange, function (nv, ov) {
                    if (nv !== ov) {
                        var c = 'change-up';
                        $animate.addClass(elem, c, function () {
                            $animate.removeClass(elem, c);
                        });
                    }
                });
            };
        })
        .directive('blacklist', function () {
            return {
                require: 'ngModel',
                link: function (scope, elem, attr, ngModel) {
                    //OK get all Userstory from this Project
                    // OK Ajax again   
                    if (attr.blacklist === "checkUserstory") {
                        var USname = getAllUSName(scope.userstories).split('~/~');
                        ngModel.$parsers.unshift(function (value) {
                            ngModel.$setValidity('blacklist', USname.indexOf(value) === -1);
                            return value;
                        });
                    }
                    else if (attr.blacklist === "checkUserstory") {
                        var USname = getAllUSName(scope.userstories).split('~/~');
                        ngModel.$parsers.unshift(function (value) {
                            ngModel.$setValidity('blacklist', USname.indexOf(value) === -1);
                            return value;
                        });
                    }
                    else if (attr.blacklist === "checkScrumColumn") {
                        var USname = getAllUSName(scope.scrumBoardColumns).split('~/~');
                        ngModel.$parsers.unshift(function (value) {
                            ngModel.$setValidity('blacklist', USname.indexOf(value) === -1);
                            return value;
                        });
                    }

                    else if (attr.blacklist === "checkScrumColumntask") {
                        var USname = getAllTasksName(scope.userstoryChoose.tasks).split('~/~');
                        ngModel.$parsers.unshift(function (value) {
                            ngModel.$setValidity('blacklist', USname.indexOf(value) === -1);
                            return value;
                        });
                    }

                }
            };
        })
        .directive('columnList', function () {
            return {
                require: 'ngModel',
                link: function (scope, elem, attr, ngModel) {
                    //OK get all Userstory from this Project
                    // OK Ajax again   
                    var USname = getAllUSName(scope.userstories).split('~/~');
                    ngModel.$parsers.unshift(function (value) {
                        ngModel.$setValidity('columnList', USname.indexOf(value) === -1);
                        return value;
                    });
                }
            };
        })
        .directive('datetimez', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ngModelCtrl) {
                    element.datetimepicker({
                        dateFormat: 'dd-MM-yyyy',
                        language: 'en',
                        pickTime: false,
                        startDate: '01-11-2013', // set a minimum date
                        endDate: '01-11-2030'          // set a maximum date
                    }).on('changeDate', function (e) {
                        ngModelCtrl.$setViewValue(e.date);
                        scope.$apply();
                    });
                }
            };
        })
        .directive('blacklistSprint', function () {
            return {
                require: 'ngModel',
                link: function (scope, elem, attr, ngModel) {
                    var projectId = attr.blacklist;
                    console.log("US name from: ");
                    console.log(scope);
                    //OK get all Userstory from this Project
                    // OK Ajax again   
                    var SprintName = getAllUSName(scope.userstories).split('~/~');
                    ngModel.$parsers.unshift(function (value) {
                        ngModel.$setValidity('blacklist', SprintName.indexOf(value) === -1);
                        return value;
                    });
                }
            };
        })
        .directive('notification', function ($timeout) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    ngModel: '='
                },
                template: '<div class="alert fade" bs-alert="ngModel"></div>',
                link: function (scope, element, attrs) {
                    $timeout(function () {
                        element.remove();
                    }, 5000);
                }
            };
        })
        .directive('selectpicker', ['$parse', function ($parse) {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        element.selectpicker($parse(attrs.selectpicker)());
                        element.selectpicker('refresh');

                        scope.$watch(attrs.ngModel, function (newVal, oldVal) {
                            scope.$parent[attrs.ngModel] = newVal;
                            scope.$evalAsync(function () {
                                if (!attrs.ngOptions || /track by/.test(attrs.ngOptions))
                                    element.val(newVal);
                                element.selectpicker('refresh');
                            });
                        });
                        scope.$on('$destroy', function () {
                            scope.$evalAsync(function () {
                                element.selectpicker('destroy');
                            });
                        });
                    }
                };
            }])
        .directive('stickyNote', function (socket) {
            var linker = function (scope, element, attrs) {


                socket.on('onTaskMoved', function (data) {
                    // Update if the same note
                    if (data.id === scope.note.id) {
                        element.animate({
                            left: data.x,
                            top: data.y
                        });
                    }
                });

                // Some DOM initiation to make it nice
                element.css('left', '10px');
                element.css('top', '50px');
                element.hide().fadeIn();
            };

            var controller = function ($scope) {
                // Incoming


                /// Outgoing
                $scope.updateTask = function (task) {
                    socket.emit('updateTask', task);
                };

                $scope.deleteTask = function (taskId) {
                    $scope.ondelete({
                        taskId: taskId
                    });
                };
            };

            return {
                restrict: 'A',
                link: linker,
                controller: controller,
                scope: {
                    task: '=',
                    ondelete: '&'
                }
            };
        })
        .directive('focusMe', function ($timeout, $parse) {
            return {
                link: function (scope, element, attrs) {
                    var model = $parse(attrs.focusMe);
                    scope.$watch(model, function (value) {
                        console.log('value=', value);
                        if (value === true) {
                            $timeout(function () {
                                element[0].focus();
                            });
                        }
                    });
                    element.bind('blur', function () {
                        console.log('blur')
                        scope.$apply(model.assign(scope, false));
                    })
                }
            };
        });