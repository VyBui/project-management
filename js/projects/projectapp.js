// GLOBAL Variables
var username = "", userId = "";
var user = {
    username: "",
    userId: ""
};

// CONSSTANT
var DONE = "DONE";
var INPROGRESSING = "INPROGRESSING";
var BACKLOG = "BACKLOG";

var userstories = {};
'use strict';
function InitDashBoard() {
    //console.log("User Name: " + );
    var username = getCookie("loginUser");
    $("#userName").val(username);
    user.username = username;
    //Get User by Username
}
// PROJECT MANAGEMENT
var app = angular.module('myApp', ['ngRoute', 'ui.bootstrap', 'ngAnimate', "dndLists", "highcharts-ng", "ngTagsInput", "angucomplete-alt"]);
app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
                when('/', {
                    title: 'Projects',
                    templateUrl: 'projects/projects.html',
                    controller: 'projectsCtrl'
                }).
                when('/partials/dashboard.html', {
                    title: 'Projects',
                    templateUrl: 'projects/projects2.html',
                    controller: 'projectsCtrl'
                })
                .when('/edit/:id', {
                    title: 'ProjectEdit',
                    templateUrl: 'projects/projectEdit.html',
                    controller: 'projectsCtrl'
                })
                .when('/detail/:id', {
                    templateUrl: 'projects/projectDetail.html',
                    controller: 'projectDetail'})
                .when('/detail/:id/:userId', {
                    templateUrl: 'projects/projectDetail.html',
                    controller: 'projectDetail'
                })
                .when('/detailUser', {
                    title: 'User Profile',
                    templateUrl: 'user/userProfile.html',
                    controller: 'UserMemberCtrl'
                })
                .when('/backlog/:projectId', {
                    title: "Product Backlog",
                    templateUrl: '../partials/milestone/productbacklog.html',
                    controller: 'productBacklogCtrl'
                })
                .when('/sprint/:projectId', {
                    title: "Sprint Backlog",
                    templateUrl: '../partials/sprint/sprints.html',
                    controller: 'sprintBacklogCtrl'
                })
                .when('/scrumboardcolumn/:id', {
                    title: 'Scrum Board',
                    templateUrl: 'scrumboardcolumn/scrumboard.html',
                    controller: 'scrumBoardCtrl'
                })

                .otherwise({
                    title: '404 Page not found',
                    templateUrl: 'notfound.html',
                    controller: DefaultCtrl
                });
    }])
        .filter('filterTaskByNameEstimatePriority', function () {
            return function (tasks, tags) {
                if (tags.length === 0) {
                    return tasks;
                }
                else {
                    var filtered = [];
                    if (tags.length >= 2) {
                        /// And condition
                        var index = tags.length;
                        (tasks || []).forEach(function (task) {
                            var matches = true;
                            var match = function (task, tagName) {
                                var regex = new RegExp(tagName.text, 'i');
                                return task.taskName.toString().search(regex) === 0 ||
                                        task.status.toString().search(regex) === 0 ||
                                        task.estimateTime.toString().search(regex) === 0;
                            };

                            tags.forEach(function (tagName) {
                                matches = matches && match(task, tagName);
                            });
                            if (matches) {
                                filtered.push(task);
                            }
                        });
                    }
                    else {
                        // Or condition
                        (tasks || []).forEach(function (task) {
                            var matches = tags.some(function (tagName) {
                                return (angular.lowercase(task.taskName).indexOf(angular.lowercase(tagName.text)) > -1) ||
                                        (angular.lowercase(task.estimateTime).indexOf(angular.lowercase(tagName.text)) > -1) ||
                                        (angular.lowercase(task.priority).indexOf(angular.lowercase(tagName.text)) > -1) ||
                                        (angular.lowercase(task.status).indexOf(angular.lowercase(tagName.text)) > -1);
                            });
                            if (matches) {
                                filtered.push(task);
                            }
                        });
                    }
                    return filtered;
                }
            };

        })
        .filter('orderObjectByPosition', function () {
            return function (input, attribute) {
                if (!angular.isObject(input))
                    return input;

                var array = [];
                for (var objectKey in input) {
                    array.push(input[objectKey]);
                }

                array.sort(function (objecta, objectb) {
                    objecta = parseInt(objecta[attribute]);
                    objectb = parseInt(objectb[attribute]);
                    return objecta - objectb;
                });
                return array;

            };
        });
