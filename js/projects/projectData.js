app.factory("Data", ['$http', '$location', function ($http, $q, $location) {
        var serviceforuserstoryBase = 'http://projecttracking.com/www/ajax/projectManagement/';

        var obj = {};

        obj.getuserByUserName = function (q, object) {
            return $http({
                method: "POST",
                url: "http://projecttracking.com/www/ajax/userManagement/getUserByUserName.php",
                data: object,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (result) {
            }).error(function (response) {

            });
        };
        obj.getUserId = function (q, object) {
            return $http({
                method: "POST",
                url: "http://projecttracking.com/www/ajax/userManagement/getUserIdByUserName.php",
                data: object, // this is User name   
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (result) {
                return result;
            }).error(function (response) {
                return response || "Request failed";
            });
        };
        obj.get = function (q, object) {
            ///console.log(object);
            return $http({
                method: "POST",
                url: serviceforuserstoryBase + "getProject.php",
                data: object,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (result) {
               // console.log(result);
                return result;
            }).error(function (response) {
                return response || "Request failed";
            });
        };
        obj.post = function (q, object) {
            object.userId = user.userId;
            object.status = "IN PROGRESSING";
            return  $http({
                method: "POST",
                url: serviceforuserstoryBase + "addProject.php",
                data: object,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                return response;
            }).error(function (response) {
                return  response || "Request failed";
            });
        };
        obj.putUser = function (q, object) {
            console.log(object);
            return $http({
                method: "POST",
                url: "http://projecttracking.com/www/ajax/userManagement/updateUser.php",
                data: object,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {

            }).error(function (response) {

            });
        };
        obj.put = function (q, object) {
            console.log(object);
            return $http({
                method: "POST",
                url: serviceforuserstoryBase + "updateProject.php",
                data: object,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (result) {
                console.log(result);
                return result;
            });
        };
        obj.delete = function (q) {
            return $http.delete(serviceforuserstoryBase + q).then(function (results) {
                return results.data;
            });
        };
        obj.getProjectByProjectID = function (q, projectId) {
            //console.log("getProjectByProjectID " + projectId);  
            return $http({
                method: "POST",
                url: serviceforuserstoryBase + "getProjectInformationByProjectId.php",
                data: projectId,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                return response;
            }).error(function (response) {
                return  response || "Request failed";
            });
        };
        obj.getProjectByProjectID2 = function (q, project) {
            return $http({
                method: "POST",
                url: serviceforuserstoryBase + "getProjectByProjectId.php",
                data: project,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                //console.log(project);
                return response;
            }).error(function (response) {

            });
        };
        obj.createDefaultScrumBoardColumn = function (q, projectId) {
            return $http({
                method: "POST",
                url: "http://projecttracking.com/www/ajax/scrumBoardManagement/addScrumBoardDefault.php",
                data: projectId,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                return response;
            }).error(function (response) {
                // handle Error
            });
        };
        obj.GetUserExceptionNotJoinedInthisProject = function (q, project) {
            return $http({
                method: "POST",
                url: "http://projecttracking.com/www/ajax/userManagement/GetUserExceptionNotJoinedInthisProject.php",
                data: project,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (result) {
                return result;
            }).error(function (data, status) {
                //$scope.data = data || "Request failed";
                //$scope.status = status;
                return data || "Request failed";
            });
        };
        obj.addMemberToThisProject = function (q, selectedPerson) {
            //console.log(selectedPerson);
            return $http({
                method: "POST",
                url: "http://projecttracking.com/www/ajax/projectManagement/addMemberToThisProject.php",
                data: selectedPerson,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (result) {
                return result;
            }).error(function (result) {
                return result;
            });
        };
        obj.getAllUserInProject = function (q, project) {
            return $http({
                method: "POST",
                url: "http://projecttracking.com/www/ajax/projectManagement/getMemberOfThisProject.php",
                data: project,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (result) {
                return result;
            }).error(function (responseData) {

            });
        };
        obj.RemoveUserMember = function (q, usermember) {
            console.log(usermember);
            return $http({
                method: "POST",
                url: "http://projecttracking.com/www/ajax/projectManagement/removeUserMember.php",
                data: usermember,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                return response;
            }).error(function (exception) {
                /// handle exception
            });
        };
        obj.EditUserRole = function (q, member) {
            console.log(member);
            return $http({
                method: "POST",
                url: "http://projecttracking.com/www/ajax/projectManagement/editUserRole.php",
                data: member,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (dataresult) {
                console.log(dataresult);
                return dataresult;
            }).error(function (handle) {

            });
        };
        return obj;
    }])
        // USER STORIES MANAGEMENT
        .factory("productBacklogData", ['$http', '$location', function ($http, $q, $location) {

                //getUserstoryByProjectId
                var serviceforuserstoryBase = 'http://projecttracking.com/www/ajax/userstoryManagement/';

                var userstoryobj = {};

                userstoryobj.getUserstoryByProjectId = function (q, projectIdanduserId) {
                    return $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "getUserstoryByProjectId.php",
                        data: projectIdanduserId, // this is User name   
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {
                        return response || "Request failed";
                    });
                };


                // Add new userstory
                userstoryobj.post = function (q, userstoryobject) {
                    return  $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "addUserstory.php",
                        data: userstoryobject,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (response) {
                        return response;
                    }).error(function (response) {
                        return  response || "Request failed";
                    });
                };

                // Edit an exist userstory
                userstoryobj.put = function (q, userstoryobject) {
                    //console.log("userstory truoc khi dua len: ");
                    //console.log(userstoryobject);
                    return $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "updateUserstory.php",
                        data: userstoryobject,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {
                        return response || "Request failed";
                    });
                };

                // Delete
                userstoryobj.delete = function (q) {
                    return $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "deleteUserstory.php",
                        data: q,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        console.log(result);
                    }).error(function (response) {
                        return response || "Request failed";
                    });
                };

                userstoryobj.getSprint = function (q, projectId) {
                    return $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "getSprint.php",
                        data: projectId,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {

                    });
                };

                userstoryobj.getSprintBySprintId = function (q, sprintId) {
                    return $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "getSprintBySprintId.php",
                        data: sprintId,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {

                    });
                };

                return userstoryobj;

            }])
        // SPRINT BACKLOG MANAGEMENT
        .factory("sprintBacklogData", ['$http', '$location', function ($http, $q, $location) {

                //getUserstoryByProjectId
                var serviceforuserstoryBase = 'http://projecttracking.com/www/ajax/sprintManagement/';

                var sprintobj = {};

                sprintobj.getSprintByProjectId = function (q, projectIdanduserId) {
                    return $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "getSprintByProjectId.php",
                        data: projectIdanduserId, // this is User name   
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {
                        return response || "Request failed";
                    });
                };


                // Add new userstory
                sprintobj.post = function (q, sprintobject) {
                    return  $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "addNewSprint.php",
                        data: sprintobject,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (response) {
                        return response;
                    }).error(function (response) {
                        return  response || "Request failed";
                    });
                };

                // Edit an exist userstory
                sprintobj.put = function (q, sprintobject) {
                    return $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "updateSprint.php",
                        data: sprintobject,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        //console.log(result);
                        return result;
                    }).error(function (response) {
                        return response || "Request failed";
                    });
                };

                // Delete
                sprintobj.delete = function (q, sprintId) {
                    return $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "deleteSprint.php",
                        data: sprintId,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        //console.log(result);
                    }).error(function (response) {
                        return response || "Request failed";
                    });
                };
                
                sprintobj.ArchieveSprint = function(q, sprintId) {
                    return $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "archieveSprintAndUserstories.php",
                        data: sprintId,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function(result) {
                        console.log(result);
                    }).error(function(response) {
                         return response || "Request failed";
                    });
                };

                sprintobj.getUserStoryBelongSprintBySprintId = function (q, sprintId) {
                    return $http({
                        method: "POST",
                        url: serviceforuserstoryBase + "getUserstoryBySprintId.php",
                        data: sprintId
                    }).success(function (response) {

                    }).error(function (response) {

                    });
                };

                return sprintobj;



            }])
        //SCRUMBOARD MANAGEMENT
        .factory("ScrumBoardData", ['$http', '$location', function ($http, $q, $location) {
                var serviceforScrumColumnBase = 'http://projecttracking.com/www/ajax/scrumBoardManagement/';

                var scrumcolumnobj = {};

                scrumcolumnobj.getAllScrumBoardByProjectId = function (q, projectId) {
                    return $http({
                        method: "POST",
                        url: serviceforScrumColumnBase + "getAllScrumBoardByProjectId.php",
                        data: projectId, // this is User name   
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {
                        return response || "Request failed";
                    });
                };
                scrumcolumnobj.addTask = function (q, object) {
                    return $http({
                        method: "POST",
                        url: serviceforScrumColumnBase + "addTask.php",
                        data: object,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {

                    }).error(function (response) {

                    });
                };
                scrumcolumnobj.getAllTaskBelongSprintWithUserStoryId = function (q, userStoryId) {
                    return $http({
                        method: "POST",
                        url: serviceforScrumColumnBase + "getAllTaskBelongSprintWithUserStoryId.php",
                        data: userStoryId,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        // console.log(result);
                        return result;
                    }).error(function (response) {

                    });
                };
                scrumcolumnobj.updateTaskbyTaskId = function (q, object) {
                    //console.log("update right here");
                    return $http({
                        method: "POST",
                        url: serviceforScrumColumnBase + "updateTaskByTaskId.php",
                        data: object,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {

                    });
                };
                scrumcolumnobj.deleteTaskbyTaskId = function (q, object) {
                    return $http({
                        method: "POST",
                        url: serviceforScrumColumnBase + "deleteTaskByTaskId.php",
                        data: object,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        // console.log(result);
                        return result;
                    }).error(function (response) {

                    });
                };
                scrumcolumnobj.addScrumColumn = function (q, object) {
                    return $http({
                        method: "POST",
                        url: serviceforScrumColumnBase + "addScrumColumn.php",
                        data: object,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {

                    });
                };
                scrumcolumnobj.updateScrumBoardColumn = function (q, object) {
                    return $http({
                        method: "POST",
                        url: serviceforScrumColumnBase + "updateScrumBoardColumn.php",
                        data: object,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {
                        /// handle Error
                    });
                };
                scrumcolumnobj.getAllTaskByScrumBoardId = function (q, object) {
                    return $http({
                        method: "POST",
                        url: serviceforScrumColumnBase + "getAllTaskByScrumBoardId.php",
                        data: object,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {
                        return response || "Request failed";
                    });
                };
                scrumcolumnobj.getUserMemberBelongToTaskId = function (q, object) {
                    return $http({
                        method: "POST",
                        url: serviceforScrumColumnBase + "getUserMemberBelongToTaskId.php",
                        data: object,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        //console.log(result);
                        return result;
                    }).error(function (response) {
                        return response || "Request failed";
                    });
                };
                scrumcolumnobj.removeUserFromTask = function (q, object) {
                    return $http({
                        method: "POST",
                        url: serviceforScrumColumnBase + "removeUserFromTask.php",
                        data: object,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {

                    });
                };

                scrumcolumnobj.addUserToTask = function (q, object) {
                    return $http({
                        method: "POST",
                        url: serviceforScrumColumnBase + "addUserToTask.php",
                        data: object,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (response) {

                    });
                };

                scrumcolumnobj.getAllUserInProject = function (q, project) {
                    return $http({
                        method: "POST",
                        url: "http://projecttracking.com/www/ajax/projectManagement/getMemberOfThisProject.php",
                        data: project,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (result) {
                        return result;
                    }).error(function (responseData) {

                    });
                };

                return scrumcolumnobj;
            }])
        //SOCKET
        .factory('socket', function ($rootScope) {
            var socket = io.connect();
            return {
                on: function (eventName, callback) {
                    socket.on(eventName, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            callback.apply(socket, args);
                        });
                    });
                },
                emit: function (eventName, data, callback) {
                    socket.emit(eventName, data, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback.apply(socket, args);
                            }
                        });
                    });
                }
            };
        })
