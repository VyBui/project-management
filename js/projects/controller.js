// CONTROLLER
/*
 * 
 * 
 * Controller for handle DATA
 * CONSTANT 
 * 
 */
var PO = "PO";
var SM = "SM";
var TM = "TM";


app.controller('UserMemberCtrl', function ($scope, $modal, $filter, $timeout, Data) {

    //// User Management
    var user = {
        userName: "",
        userId: 0
    };

    $scope.messageUpdate = false;

    try {
        if (user.userName === "") {
            user.username = getCookie("loginUser");
        }

        /// GOT user Name
        Data.getuserByUserName('user', user).then(function (response) {
            if (response.data !== "notfound") {
                $scope.user = response.data;
                console.log($scope.user);
            }
        });
    }
    catch (ex) {
        console.log(ex);
    }

    $scope.EditUser = function (user) {
        Data.putUser('user', user).then(function (response) {
            console.log(response.data);
            if (response.data !== 'error') {
                // show message Successfully
                $scope.messageUpdate = true;

                var messageTimer = false;

                $scope.messageError = {
                    condition: true,
                    //context: "Require task Name and estimate hours"
                };
                if (messageTimer) {
                    $timeout.cancel(messageTimer);
                }
                messageTimer = $timeout(function () {
                    $scope.messageError.condition = false;
                }, 1500);
            }
        });
    };


})
        // USER MANAGEMENT WITH AUTHENTICATION AND AUTHORIZARION       
        .controller('projectsCtrl', function ($scope, $modal, $filter, sharedUserId, Data) {
            $scope.project = {};
            if (user.username === "") {
                user.username = (getCookie("loginUser"));
                $scope.userName = user.username;
            }
            else {
                $scope.userName = user.username;
            }
            if (user.userId === "") {
                // Get UserId
                Data.getUserId('user', user).then(function (data) {
                    setUserId(data.data[0]);
                    user.userId = parseInt(data.data[0]);
                    $scope.user = data.data;
                    //console.log($scope.user);
                    //console.log(user);
                    sharedUserId.setuserId(data.data[0]);
                    Data.get('projects', user).then(function (data) {
                        // $scope.projects = data.data;
                        // Arrange Based on STATUS
                        if (data.data !== null) {
                            $scope.projects = data.data;
                            $scope.projectsINPROGRESSING = $scope.getProjectInProgressing(data.data);
                            $scope.projectsPOSTPONE = getProjectPOSTPONE(data.data);
                            $scope.projectsDONE = $scope.getProjectDONE(data.data);
                            $scope.GetUserInProject($scope.projects);
                        }
                    }).finally(function () {
                        $scope.Paginate();
                    });
                });
            }
            else {
                console.log("else");
                Data.get('projects', user).then(function (data) {
                    // $scope.projects = data.data;
                    // Arrange Based on STATUS
                    if (data.data !== null) {
                        $scope.projects = data.data;
                        $scope.projectsINPROGRESSING = $scope.getProjectInProgressing(data.data);
                        $scope.projectsPOSTPONE = getProjectPOSTPONE(data.data);
                        $scope.projectsDONE = $scope.getProjectDONE(data.data);
                        $scope.project.userId = user.userId[0];
                        $scope.GetUserInProject($scope.projects);
                    }
                }).finally(function () {
                    $scope.Paginate();
                });
            }

            $scope.changeProjectStatus = function (project) {
                project.status = (project.status === "Active" ? "Inactive" : "Active");
                Data.put("projects/" + project.id, {status: project.status});
            };
            $scope.deleteProject = function (project) {
                if (confirm("Are you sure to remove the project")) {
                    Data.delete("projects/" + project.id).then(function (result) {
                        $scope.projects = _.without($scope.projects, _.findWhere($scope.projects, {id: project.id}));
                    });
                }
            };

            $scope.directtoDetail = function (project) {
                window.location.href = "dashboard.html#/detail/" + project.projectId + "/" + project.userId;
            };
            // Add new Project Function
            $scope.open = function (p, size) {
                var modalInstance = $modal.open({
                    templateUrl: 'projects/projectEdit.html',
                    controller: 'projectEditCtrl',
                    size: size,
                    resolve: {
                        item: function () {
                            return p;
                        }
                    }
                });
                modalInstance.result.then(function (selectedObject) {
                    if (selectedObject.save === "insert") {
                        $scope.InitialProject(selectedObject);
                    } else if (selectedObject.save === "update") {
                        p.description = selectedObject.description;
                        p.price = selectedObject.price;
                        p.stock = selectedObject.stock;
                        p.packing = selectedObject.packing;
                        p.isArchive = selectedObject.isArchive;
                    }
                });
            };
            $scope.columns = [
                {text: "IN PROGRESSING", predicate: "in progressing", sortable: true},
                {text: "POSTPONE", predicate: "postpone", sortable: true},
                {text: "DONE", predicate: "done", sortable: true}
            ];

            $scope.GetUserInProject = function () {
                //console.log($scope.projects); 
                if ($scope.projectsINPROGRESSING) {
                    for (var index in $scope.projectsINPROGRESSING) {
                        $scope.projectsINPROGRESSING[index].usermembers = [];
                        (function (index) {
                            Data.getAllUserInProject("project", $scope.projectsINPROGRESSING[index]).then(function (usermmeber) {
                                if (usermmeber.data[0] !== null) {
                                    $scope.projectsINPROGRESSING[index].usermembers = usermmeber.data;
                                }
                            }).finally(function () {
                            });
                        })(index);
                    }
                }
                else {

                }
            };

            /// paginate
            $scope.Paginate = function () {
                $scope.totalProjectsInProgress = $scope.projectsINPROGRESSING.length;
                $scope.totalProjectsInArchive = $scope.projectsDONE.length;
                $scope.projectsInprogressPerPage = 5;
                $scope.projectsArchivePerPage = 4;
                if (!$scope.currentPage) {
                    $scope.currentPage = 1;
                }
                if (!$scope.currentArchiveProjectPage) {
                    $scope.currentArchiveProjectPage = 1;
                }
                $scope.maxSize = 5;
                $scope.bigTotalItems = 175;
                $scope.bigCurrentPage = 1;



                $scope.$watch('currentPage + projectsInprogressPerPage', function () {
                    var begin = (($scope.currentPage - 1) * $scope.projectsInprogressPerPage),
                            end = begin + $scope.projectsInprogressPerPage;

                    $scope.filteredProjects = $scope.projectsINPROGRESSING.slice(begin, end);
                });

                $scope.$watch('currentArchiveProjectPage + projectsArchivePerPage', function () {
                    var begin = (($scope.currentArchiveProjectPage - 1) * $scope.projectsArchivePerPage),
                            end = begin + $scope.projectsArchivePerPage;
                    $scope.filteredProjectsArchive = $scope.projectsDONE.slice(begin, end);
                });
            };

            $scope.getProjectInProgressing = function (array) {
                var returnArray = Array();
                var j = 0;
                for (var i = 0; i < array.length; i++) {

                    if (array[i].status === "IN PROGRESSING" && array[i].isArchive !== "1")
                    {
                        returnArray[j] = array[i];
                        j++;
                    }
                }
                return returnArray;
            };
            $scope.getProjectDONE = function (array) {
                var returnArray = Array();
                var j = 0;
                for (var i = 0; i < array.length; i++) {
                    if (array[i].status === "DONE" || array[i].isArchive === "1")
                    {
                        returnArray[j] = array[i];
                        j++;
                    }
                }
                return returnArray;
            };
            $scope.InitialProject = function (selectProject) {
                selectProject.usermembers = [];
                Data.getAllUserInProject('project', selectProject).then(function (data) {
                    if (data.data !== 'error') {
                        selectProject.usermembers = data.data;
                        console.log(selectProject.usermembers);
                        $scope.projectsINPROGRESSING.push(selectProject);
                        $scope.Paginate();
                        $scope.projects = $filter('orderBy')($scope.projects, 'id', 'reverse');
                    }
                    ;

                });

            };

        })
        .controller('projectEditCtrl', function ($scope, $modalInstance, item, Data) {
            $scope.project = angular.copy(item);
            $scope.cancel = function () {
                $modalInstance.dismiss('Close');
            };
            $scope.title = (item.id > 0) ? 'Edit Project' : 'Add Project';
            $scope.buttonText = (item.id > 0) ? 'Update Project' : 'Add New Project';

            var original = item;

            $scope.isClean = function () {
                return angular.equals(original, $scope.project);
            };

            $scope.saveProject = function (project) {
                //console.log(project);
                if (project.projectId > 0) {
                    Data.put('projects/' + project.projectId, project).then(function (result) {
                        if (result.status !== 'error') {
                            var x = angular.copy(project);
                            x.save = 'update';
                            $modalInstance.close(x);
                        } else {
                            //  console.log(result);
                        }
                    });
                }
                else {
                    project.status = 'IN PROGRESSING';
                    Data.post('projects', project).then(function (result) {
                        var projectIDNew = result.data;
                        if (result.data !== 'error') {
                            // Create 3 Column For Scrum Board
                            Data.createDefaultScrumBoardColumn('projects', result.data).then(function (responsedata) {
                                var x = angular.copy(project);
                                //x.projectId = result.data;
                                x.save = 'insert';
                                x.projectId = projectIDNew;
                                var userId = x.userId[0];
                                x.userId = userId;
                                $modalInstance.close(x);
                            });

                        } else {
                            alert("Can not add project because You use these syntax ',* ");
                        }
                    });
                }
            };
        })
        .controller('projectDetail', function ($scope, $location, $timeout, $routeParams, Data, shareMemberInProject) {
            var project = {
                "projectId": $routeParams.id,
                "userId": $routeParams.userId
            };
            $scope.keywords;
            $scope.roleName;
            $scope.tags = [];
            $scope.searchStr = "";
            $scope.userMembers = [];
            /// get project
            Data.getProjectByProjectID2('project', project).then(function (result) {
                try {
                    //var checked = JSON.parse(result.data);
                    var tostring = result.data.toString();
                    /*/// Get Authentication from Server
                     * 
                     *
                     *
                     *IF no User with that project Id , server response notfoundproject*/
                    if (JSON.stringify(tostring) === JSON.stringify("notfoundproject")) {
                        return $location.path('/notfound');
                    }
                    // If Server reponse with empty Us
                    else if (parseInt(tostring) === 0) {
                        $location.path('/notfound');
                    }
                    /// Else everything OK
                    else {
                        $scope.project = result.data[0];
                        //   console.log($scope.project);
                        if (parseInt($scope.project.isArchive) === 1) {
                            $scope.projectCondition = "UnArchive";
                        }
                        else {
                            $scope.projectCondition = "Archived";
                        }
                        $scope.people = [
                        ];
                        return Data.getAllUserInProject('project', project);

                    }
                }
                catch (ex) {

                }

            })
                    .then(function (response) {
                        if (response.data !== "error") {
                            $scope.userMembers = response.data;
                            // console.log($scope.userMembers);
                            // set
                            shareMemberInProject.setUserMemberAndRole($scope.userMembers);
                            // find MySefl
                            $scope.mySelf = $scope.findMySelf(response.data, project.userId);
                            if ($scope.mySelf !== null && $scope.mySelf !== undefined) {
                                //console.log($scope.mySelf);
                            }
                        }
                    })
                    .finally(function () {
                        $scope.people = [];
                        return Data.GetUserExceptionNotJoinedInthisProject('project', project).then(function (data) {
                            if (data.data !== 'error') {
                                if (data.data.length > 0)
                                {
                                    for (var i in data.data) {
                                        //console.log(data.data[i]);
                                        $scope.people.push(data.data[i]);
                                    }
                                }
                            }
                        });
                    });
            // DISABLE DAY
            // date-disabled="disabled(date, mode)" 
            // THIS WAS VERY USEFULLL :D
            $scope.disabled = function (date, mode) {
                return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
            };
            // TOGGLE MIN
            $scope.toggleMin = function () {
                $scope.minDate = $scope.minDate ? null : new Date();
            };
            $scope.toggleMin();
            $scope.openProjectStart = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.openedProjectStart = true;
            };
            $scope.openStartInteractive = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.openedStartInteractive = true;
            };

            $scope.openEndDate = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.EndDateopened = true;
            };
            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };
            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            // UPDATE PROJECT
            $scope.saveProject = function (project) {
                if (project.projectId > 0) {
                    Data.put('projects/' + project.id, project).then(function (result) {
                        // console.log(project);
                        if (result.data !== 'error') {
                            //console.log(project);
                            var x = angular.copy(project);
                            x.save = 'update';
                            location.reload();
                        } else {
                        }
                    });
                }
            };
            // FIND MY SELF IN PROJECT
            $scope.findMySelf = function (array, userId) {
                if (array.length) {
                    for (var index in array) {
                        if (array[index].userId === userId) {
                            return angular.copy(array[index]);
                            break;
                        }
                    }
                }
                return null;
            };
            //DELETE PROJECT
            $scope.changeProjectCondition = function (project) {
                $scope.projectCondition = ($scope.projectCondition === "UnArchive" ? "Archived" : "UnArchive");
                if ($scope.projectCondition === "UnArchive") {
                    $scope.project.isArchive = "1";
                }
                else {
                    $scope.project.isArchive = "0";
                }
                console.log($scope.project.isArchive);
            };
            //GO TO PRODUCT BACKLOG
            $scope.gotoProductBacklog = function (projectId) {
                window.location.href = "#/backlog/" + projectId;
            };
            // ADD MEMBER TO PROJECT
            $scope.addMembertoProject = function (selectPerson, mySelf) {
                //console.log(selectPerson);      
                var messageTimer = false;
                if (messageTimer) {
                    $timeout.cancel(messageTimer);
                }
                try {
                    /// If mySelf is PO, I have all right to add new member
                    if (mySelf.roleName === PO) {
                        console.log("OK, you are PO, You have all right to add new mem and set Roles");
                        if (selectPerson !== undefined) {
                            return $scope.AddThisPersonToProject(selectPerson);
                        }
                        else {
                            $scope.CallAlertForHelp("danger", "Oh wait! we can not find the data!", 5000);
                        }
                    }
                    else if (mySelf.roleName === SM) {
                        console.log("Well, You're Scrum Master, You only, add newber and just one right set TM");
                        if (selectPerson !== undefined) {
                            if (selectPerson.roleName === PO) {
                                // not allowed
                                return $scope.CallAlertForHelp('danger', "You don't have the right to add new PO member in this project", 3000);
                            }
                            else {
                                /// Alllows
                                return $scope.AddThisPersonToProject(selectPerson);
                            }
                        }
                        else {
                            $scope.CallAlertForHelp("danger", "Oh wait! we can not find the data!", 5000);
                        }
                    }
                    else {
                        console.log("You can add new member but dont have the right to set Role, Sorry about that");
                        if (selectPerson !== undefined) {
                            if (selectPerson.roleName === PO || selectPerson.roleName === SM) {
                                // Not allow
                                return $scope.CallAlertForHelp('danger', "You don't have the right to add new " + selectPerson.roleName + " member in this project", 5000);
                            }
                            else {
                                // Allows
                                return $scope.AddThisPersonToProject(selectPerson);
                            }
                        }
                        else {
                            $scope.CallAlertForHelp("danger", "Oh wait! we can not find the data!", 5000);
                        }
                    }
                } catch (ex) {

                }
            };
            /// Remove Member
            $scope.RemoveMember = function (member, mySelf) {
                //var messageTimer = false;
                // My sellf is Product Owner
                if (mySelf.roleName === PO) {
                    if (member.roleName === PO) {
                        var countPO = countProductOwner($scope.userMembers, PO);
                        if (countPO > 1) {
                            // ///cho phep
                            /// http request
                            return $scope.RemovedUserMember(member);
                        }
                        else if (countPO === 1) {
                            $scope.CallAlertForHelp("danger", "Warning, there are at least one Product Owner, who take in charge this project!!!!!!!", 5000);
                        }
                        else {
                            /// I dont know
                        }
                    }
                    else {
                        /// Delete thoai mai
                        return $scope.RemovedUserMember(member);
                    }
                }
                // Scrum master
                else if (mySelf.roleName === SM) {
                    if (member.roleName !== PO) {
                        // Allow
                        return $scope.RemoveMember(member);
                    }
                    else {
                        // not allow
                        $scope.CallAlertForHelp('danger', "you don't have the right to remove PO", 5000);
                    }
                }
                /// Deverloper
                else {
                    $scope.CallAlertForHelp('danger', "you need PO or SM right to remove this person", 5000);
                }
            };
            ///  Customise Alert
            $scope.CallAlertForHelp = function (type, message, durations) {
                var messageTimer = false;
                $scope.alert = {
                    type: type,
                    msg: message,
                    condition: true
                };
                messageTimer = $timeout(function () {
                    $scope.alert.condition = false;
                }, durations);
            };
            //// Get User not join this project
            $scope.getUserNotJoinedThisProjectAgain = function () {
                Data.GetUserExceptionNotJoinedInthisProject('project', project).then(function (data) {
                    //$scope.people = [];
                    if (data.data !== 'error') {
                        $scope.people = data.data;
                    }
                });
            };
            /// add select person to this project
            $scope.AddThisPersonToProject = function (selectPerson) {
                selectPerson.projectId = project.projectId;
                /// Finally add user members
                Data.addMemberToThisProject('project', selectPerson).then(function (response) {
                    //onsole.log(response);
                    if (response.data !== "error") {
                        $scope.userMembers.push(selectPerson);
                        $scope.CallAlertForHelp("success", "Excellent! Add new member successfully!", 5000);
                        var index = $scope.people.indexOf(selectPerson);
                        $scope.people.slice(index, 1);
                        //console.log("selectedPerson luc nay");
                        console.log($scope.selectedPerson);
                    }
                });
            };
            /// remove person
            $scope.RemovedUserMember = function (member) {
                Data.RemoveUserMember('project', member).then(function (responseData) {
                    if (responseData.data !== "error") {
                        $scope.userMembers = _.without($scope.userMembers, _.findWhere($scope.userMembers, {userId: member.userId}));
                        $scope.CallAlertForHelp("success", "Well done!. the user member has been removed of this project", 5000);
                        $scope.getUserNotJoinedThisProjectAgain();
                    }
                    else {

                    }
                });
            };
            // stop follow this project
            $scope.stopFollow = function (myself) {
                // check to figure out how many PO in this project
                var countPO = countProductOwner($scope.userMembers, PO);
                if (myself.roleName === PO) {
                    if (countPO > 1) {
                        Data.RemoveUserMember('project', myself).then(function (responseData) {
                            if (responseData.data !== "error") {
                                // go to home page
                                $location.path('/');
                            }
                        });
                    }
                    else {
                        return $scope.CallAlertForHelp("danger", "Warning, there are at least one Product Owner, who take in charge this project!, \n\
                    You cannot remove the last owner on a project, since all projects need at least one owner.", 5000);
                    }
                }
                else {
                    Data.RemoveUserMember('project', myself).then(function (responseData) {
                        if (responseData.data !== "error") {
                            // go to home page
                            $location.path('/');
                        }
                    });
                }

            };

            $scope.EditRole = function (member, mySelf) {
                // if myself is PO, I have all right to edit Role
                // If I'm SM, I wanted to set PO?, edit Role                
                if (mySelf.roleName === PO) {
                    ///Have all right to do
                    return $scope.CallEditUserRole(member);
                }
                else if (mySelf.roleName === SM) {
                    /// think about it later
                }
                else {
                    /// NO please
                    return $scope.CallAlertForHelp("danger", "No, You need PO role to set new role for this Person", 5000);
                }
            };

            $scope.UpdateMyRole = function (myself) {
                var countPO = countProductOwner($scope.userMembers, PO);
                console.log($scope.userMembers);
                console.log(countPO);
                if (myself.roleName === PO) {
                    if (countPO > 1) {
                        // cho phep edit Role
                        return $scope.CallEditUserRole(myself);
                    }
                    else {
                        // khong cho phep
                        console.log($scope.mySelf);
                        $scope.mySelf.roleName = PO;
                        return $scope.CallAlertForHelp("danger", "You can not remove the last product owner on a project, since all projects need at least one owner", 5000);
                    }
                }
                else {
                    return $scope.CallAlertForHelp("danger", "You need PO role to update your role", 5000);
                }

            };

            $scope.CallEditUserRole = function (member) {
                member.projectId = project.projectId;
                Data.EditUserRole('project', member).then(function (responseData) {
                    if (responseData.data !== "error") {

                    }
                    else {

                    }
                });
            };
        })
        .controller('productBacklogCtrl', function ($scope, $modal, $filter, $location, $routeParams, productBacklogData, Data, sprintBacklogData, sharedUserId) {
            $scope.userstories = {};
            /*
             * userstvarory and userstories global 
             * 
             */
            var projectIdandUserId = {};
            $scope.userstory = {};
            var userstoryPoint = {};
            $scope.orderByField = 'priority';
            $scope.reverseSort = false;
            $scope.Userstorycolumns = [
                {text: "Priority", predicate: "priority", sortable: true},
                {text: "ID", predicate: "id", sortable: true, reverse: true, dataType: "number"},
                {text: "Name", predicate: "name", sortable: true},
                {text: "Type", predicate: "description", sortable: true},
                {text: "US-Point", predicate: "point", sortable: true},
                {text: "Status", predicate: "status", sortable: true},
                {text: "Sprint", predicate: "sprint", reverse: true, sortable: true, dataType: "number"},
                {text: "Action", predicate: "", sortable: false}
            ];
            var projectId = $routeParams.projectId;
            $scope.projectId = $routeParams.projectId;
            $scope.orderByField = 'Priority';
            $scope.reverseSort = false;
            // SET USERNAME    
            if (user.username === "") {
                user.username = getCookie("loginUser");
            }


            // GET USERID
            if (user.userId === "" || user.userId !== "") {
                // USING PROMISE TO DO
                Data.getUserId('user', user).then(function (data) {
                    // GET USER ID
                    //if()
                    console.log(data.data[0][0]);
                    projectIdandUserId = {
                        "projectId": projectId,
                        "userId": data.data[0][0]
                    };
                    sharedUserId.setuserId(data.data[0]);
                    return productBacklogData.getUserstoryByProjectId('userstory', projectIdandUserId);
                }).then(function (data) {
                    console.log(data.data);
                    var tostring = data.data.toString();
                    /*/// Get Authentication from Server
                     * 
                     *
                     *
                     *IF no User with that project Id , server response notfoundproject*/
                    if (tostring === JSON.stringify("notfoundproject")) {
                        return $location.path('/notfound');
                    }
                    // If Server reponse with empty Us
                    else if (tostring === JSON.stringify("nodata")) {
                        $scope.userstories = [];
                    }
                    /// Else everything OK
                    else {
                        try {
                            /// GET ALL USERSTORIES     
                            $scope.userstories = data.data;
                            /*
                             * FILTER USER STORY NAME
                             * 
                             */
                            $scope.allUSName = getAllUSName(data.data);
                            /*
                             * 
                             */
                            userstories = data.data;
                            /*
                             * 
                             */
                            /*
                             * DRAW BURNT-DOWN CHART
                             */
                            if ($scope.userstories.length > 0)
                            {
                                userstoryPoint = calculatePoint($scope.userstories);
                                DrawChart(userstoryPoint);
                            }
                        }
                        catch (ex) {
                            console.log(ex);
                        }

                    }
                    return sprintBacklogData.getSprintByProjectId('sprint', projectIdandUserId);
                }).then(function (data) {
                    if (data.data !== null) {
                        $scope.sprintsOfProject = data.data;
                    }
                    getSprintNameForUS($scope);
                    return Data.getProjectByProjectID2('project', projectIdandUserId);
                }).then(function (projectData) {
                    console.log(projectData.data);
                    $scope.projectInformation = projectData.data[0];
                });
            }
            // Get sproject Information

            // Get sprint has status "TODO" in this project
            //             
            // open modal create new User Story
            $scope.open = function (userstory) {
                userstory.projectId = projectId;
                var modalInstance = $modal.open({
                    templateUrl: 'milestone/createNewUserstory.html',
                    controller: 'userStoryEditCtrl',
                    size: 'lg',
                    resolve: {
                        item: function () {
                            return userstory;
                        },
                        userstories: function () {
                            return userstories;
                        }
                    }
                });
                modalInstance.result.then(function (selectedObject) {
                    if (selectedObject.save === "insert") {
                        $scope.userstories.push(selectedObject);
                        $scope.userstories = $filter('orderBy')($scope.userstories, 'id', 'reverse');
                    } else if (selectedObject.save === "update") {
                        userstory.description = selectedObject.description;
                        userstory.point = selectedObject.point;
                        userstory.name = selectedObject.name;
                        userstory.status = selectedObject.status;
                        userstory.sprintName = selectedObject.sprintName;
                        userstory.type = selectedObject.type;
                        userstory.priority = selectedObject.priority;
                    }
                });
            };
            $scope.openDelete = function (userstory) {
                $scope.userstory = userstory;
                var modalInstance = $modal.open({
                    templateUrl: 'milestone/deleteUserstory.html',
                    controller: 'deleteUserstoryCtrl',
                    size: 'sm',
                    resolve: {
                        item: function () {
                            return userstory;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    $scope.userstories = _.without($scope.userstories, _.findWhere($scope.userstories, {userStoryId: result}));
                });
            };
        })
        .controller('userStoryEditCtrl', function ($scope, $modalInstance, item, userstories, productBacklogData) {
            productBacklogData.getSprint('userstory', item.projectId)
                    .then(function (data) {
                        console.log(data.data);
                        $scope.sprintsOfProject = data.data;
                        $scope.saveUserstory = function (userstory) {
                            $scope.messageError = {
                                condition: false,
                                context: ""
                            };
                            console.log(userstory);
                            if (userstory.sprintId === "0") {
                                userstory.sprintName = "";
                            }
                            if (userstory.userStoryId > 0) {
                                // check if Userstory don't have Sprint but Status != TODO
                                if ((userstory.sprintId < 0 || userstory.sprintId === "0" || userstory.sprintId === null) && userstory.status !== "TODO") {
                                    $scope.messageError.condition = true;
                                    $scope.messageError.context = "An exist Userstory need a sprint before set up status";
                                }
                                else {
                                    productBacklogData.put('userstorys/' + userstory.userStoryId, userstory).then(function (result) {
                                        if (result.data !== 'error')
                                        {
                                            var x = angular.copy(userstory);
                                            x.save = 'update';
                                            if (x.sprintId === "0")
                                            {
                                                x.sprintName = "";
                                            }
                                            else {
                                                x.sprintName = GetSprintNameById($scope, x.sprintId);
                                            }
                                            $modalInstance.close(x);
                                        } else {
                                            console.log(result);
                                        }
                                    });
                                }
                            } else {
                                userstory.status = 'TODO';
                                productBacklogData.post('userstorys', userstory).then(function (result) {
                                    console.log(result.data);
                                    if (result.data !== 'error') {
                                        var x = angular.copy(userstory);
                                        x.save = 'insert';
                                        x.userStoryId = result.data;
                                        $modalInstance.close(x);
                                    }
                                    else if (result.data === "duplicate") {
                                        alert("this project already have that Userstory name, ");
                                    }
                                    else {
                                        alert("Can not add userstory because, I don't know");
                                    }
                                });
                            }
                        };
                    });
            $scope.userstory = angular.copy(item);
            $scope.userstories = angular.copy(userstories);
            $scope.cancel = function () {
                $modalInstance.dismiss('Close');
            };
            $scope.title = (item.userStoryId > 0) ? 'Edit Userstory' : 'Add Userstory';
            $scope.buttonText = (item.userStoryId > 0) ? 'Update Userstory' : 'Add New Userstory';
            $scope.isDisabled = (item.userStoryId > 0) ? false : true;
            $scope.displayStatus = (item.userStoryId > 0) ? "block" : "none";
            var original = item;
            $scope.isClean = function () {
                return angular.equals(original, $scope.userstory);
            };
        })
        .controller('deleteUserstoryCtrl', function ($scope, $modalInstance, item, productBacklogData) {
            $scope.userstory = angular.copy(item);
            var deleteId = $scope.userstory.userStoryId;
            $scope.cancel = function () {
                $modalInstance.dismiss('Close');
            };
            $scope.deleteUserstory = function () {
                productBacklogData.delete($scope.userstory.userStoryId).then(function (result) {
                    //$scope.userstories = _.without($scope.userstories, _.findWhere($scope.userstories, {userStoryId: userstory.userStoryId}));
                    if (result.data === "success") {
                        $modalInstance.close(deleteId);
                        location.reload();
                    }
                });
            };
        })
        ///////// SPRINT MANAGEMENT
        .controller("sprintBacklogCtrl", function ($scope, $modal, $location, $filter, sharedUserId, $routeParams, sprintBacklogData, productBacklogData, Data) {
            $scope.conditionToggle = {};
            initialToggle($scope);

            $scope.projectId = $routeParams.projectId;
            // Defined new sprint
            $scope.sprint = {};
            // Sprints Content all Sprint in project
            $scope.sprints = {};
            // Classified depend on Status of sprint In BACKLOG
            $scope.sprintBackLog = [];
            // Classified depend on Status of sprint In PROGRESSING
            $scope.sprintInProgress = [];
            // Classified depend on Status of sprint DONE
            $scope.sprintDone = [];

            $scope.userstoriesInCurrentBacklog = {};

            //var arrayforWidthTable = [1068,1300];
            $scope.userstoryType = ["userstory"];
            //console.log($scope.userstoryType);

            // design width of table
            $scope.tableContentColumn = {
                width: 1631
            };
            if (user.username === "") {
                user.username = getCookie("loginUser");
            }
            // Get all Sprint
            var projectIdandUserId = {
                "projectId": $routeParams.projectId,
                "userId": user.userId
            };

            // GET SPRINT by ProjectID
            $scope.loadingSprintPage = true;
            Data.getUserId('user', user).then(function (data) {
                projectIdandUserId.userId = data.data[0][0];
                sharedUserId.setuserId(data.data[0][0]);
                return sprintBacklogData.getSprintByProjectId('sprint', projectIdandUserId);
            })
                    .then(function (data) {
                        var tostring = data.data.toString();
                        /*/// Get Authentication from Server
                         * 
                         *
                         *
                         *IF no User with that project Id , server response notfoundproject*/
                        if (tostring === JSON.stringify("notfoundproject")) {
                            return $location.path('/notfound');
                        }
                        // If Server reponse with empty Us
                        else if (tostring === JSON.stringify("nodata")) {
                            $scope.sprints = [];
                        }
                        /// Else everything OK
                        else {
                            // Get sprint in this project
                            $scope.sprints = data.data;
                            // Generate type.
                            $scope.sprints.dataType = ["sprint"];
                            for (var i = 0; i < $scope.sprints.length; i++)
                            {
                                $scope.sprints[i].userstories = [];
                            }
                            //Load US belong Sprint
                            // Nothing work because It's not Synchronize       
                            // GET all Userstory 
                            productBacklogData.getUserstoryByProjectId('userstory', projectIdandUserId).then(function (data) {
                                $scope.productbacklogUserstory = data.data;
                                /// Arrange Userstory
                                ArrageUserstory($scope);
                                // Watch for sprintBACKLOG
                                $scope.$watch("userstoriesInCurrentBacklog", function (model) {
                                    for (var i = 0; i < model.length; i++)
                                    {
                                        if (model[i].sprintId > 0)
                                        {
                                            model[i].sprintId = 0;
                                            productBacklogData.put('userstorys/' + model[i].userstoryId, model[i]).then(function (response) {

                                            }, function (error) {
                                                //console.log("handle " + error);
                                            }).finally(function (handle) {
                                               // console.log(handle);
                                            });
                                        }
                                    }
                                }, true);
                                /*
                                 * Sprint DONE on this project
                                 */
                                $scope.$watch("sprintDone", function (model) {
                                    if ($scope.sprintDone.length > 0) {
                                        for (var i = 0; i < $scope.sprintDone.length; i++)
                                        {
                                            if ($scope.sprintDone[i].status !== DONE)
                                            {
                                                $scope.sprintDone[i].status = DONE;
                                                $scope.sprintInProgress[0].isProgress = 0;
                                                sprintBacklogData.put('sprint', $scope.sprintDone[i]).then(function (response) {

                                                }, function (response) {
                                                    // Handle Error so$scope.sprintDone[i]me how
                                                }).finally(function () {

                                                });
                                            }
                                            else {
                                                $scope.Doneloading = false;
                                            }


                                            if ($scope.sprintDone[i].userstories.length > 0) {
                                                try {
                                                    var Point = calculatePoint($scope.sprintDone[i].userstories);
                                                    $scope.sprintDone[i].totalPoint = Point.totalPoint;
                                                    $scope.sprintDone[i].pointComplete = Point.completePoint;
                                                }
                                                catch (ex) {
                                                    console.log(ex);
                                                }
                                            }
                                            else {
                                                $scope.sprintDone[i].totalPoint = 0;
                                                $scope.sprintDone[i].pointComplete = 0;
                                                sprintBacklogData.put('sprint', $scope.sprintDone[i]).then(function (response) {

                                                }, function (error) {

                                                }).finally(function (ex) {
                                                    // $scope.BackLogloading = false;
                                                });
                                            }

                                            //calculate total Point
                                            // Calculate 2 dates between.
                                            if ($scope.sprintDone[i].start_date !== " " && $scope.sprintDone[i].start_date !== " ")
                                            {
                                                try {


                                                } catch (exception) {
                                                    console.log(exception);
                                                }
                                            }

                                            /// CAN MOVE :S:S:S:S:S:S:S:S:S                                   
                                            if ($scope.sprintDone[i].end_date !== "") {
                                                var current = new Date();
                                                var end_date = new Date($scope.sprintDone[i].end_date);
                                                if (current > end_date) {
                                                    $scope.sprintDone[i].isMove = false;
                                                    //  console.log(current + ">" + end_date);
                                                }
                                                else {
                                                    $scope.sprintDone[i].isMove = true;
                                                    //    console.log(current + "<" + end_date);
                                                }
                                            }


                                            /// UPDATE :@:@:@:@:@:@:@:@:@
                                            sprintBacklogData.put('sprint', $scope.sprintDone[i]).then(function (response) {

                                            }, function (error) {

                                            }).finally(function (ex) {

                                            });
                                        }

                                        // Calcualate
                                        // current date

                                    }
                                    else {
                                        $scope.Doneloading = false;
                                    }


                                }, true);
                                /*
                                 * Sprint In Progress
                                 */
                                $scope.$watch('sprintInProgress', function (model) {
                                    $scope.InProgressingloading = true;
                                    if ($scope.sprintInProgress.length > 0)
                                    {
                                        //console.log($scope.sprintInProgress[0].status);
                                        if ($scope.sprintInProgress[0].status !== INPROGRESSING)
                                        {
                                            $scope.sprintInProgress[0].status = INPROGRESSING;
                                            $scope.sprintInProgress[0].isProgress = 1;
                                            sprintBacklogData.put('sprint', $scope.sprintInProgress[0]).then(function (response) {
                                                //console.log(response);
                                                if (response.data === 'error')
                                                {
                                                    // Why error
                                                }
                                                else {
                                                }
                                                //if()
                                            }, function (response) {
                                                // Handle Error somehow
                                            }).finally(function () {
                                                $scope.InProgressingloading = false;
                                            });
                                        }
                                        else {
                                            $scope.InProgressingloading = false;

                                        }

                                        // Nerver Mind
                                        var current = new Date();
                                        var end_date = new Date($scope.sprintInProgress[0].end_date);
                                        if (current > end_date)
                                        {
                                            $scope.sprintInProgress[0].isLock = 1;
                                        }
                                        else {
                                            $scope.sprintInProgress[0].isLock = 0;
                                        }

                                        sprintBacklogData.put('sprint', $scope.sprintInProgress[0]).then(function (response) {
                                            //console.log(response);
                                            if (response.data === 'error')
                                            {

                                            }
                                            else {
                                                // console.log($scope.sprintInProgress[0]);
                                            }
                                            //if()
                                        }, function (response) {
                                            // Handle Error somehow
                                        }).finally(function () {
                                            $scope.InProgressingloading = false;
                                        });

                                        try {
                                            if (model[0].sprintId > 0)
                                            {
                                                //console.log()
                                                if (model[0].userstories.length > 0) {
                                                    for (var i = 0; i < model[0].userstories.length; i++)
                                                    {
                                                        // Check if only one userstory have another sprintId ,
                                                        if (model[0].userstories[i].sprintId !== model[0].sprintId)
                                                        {
                                                            model[0].userstories[i].sprintId = model[0].sprintId;
                                                            productBacklogData.put('userstorys/' + model[0].userstories[i].userstoryId, model[0].userstories[i]).then(function (response) {
                                                                //  console.log(response);
                                                            }, function (error) {
                                                                // Handle Error
                                                            }).finally(function () {
                                                                // 
                                                            });
                                                        }
                                                    }
                                                }
                                            }

                                            if (model[0].userstories.length > 0)
                                            {
                                                try {
                                                    var Point = calculatePoint(model[0].userstories);
                                                    model[0].totalPoint = Point.totalPoint;
                                                    model[0].pointComplete = Point.completePoint;
                                                    sprintBacklogData.put('sprint', model[0]).then(function (response) {

                                                    }, function (error) {

                                                    }).finally(function (ex) {

                                                    });

                                                }
                                                catch (ex) {
                                                    console.log(ex);
                                                }
                                            }
                                            else {
                                                model[0].totalPoint = 0;
                                                model[0].pointComplete = 0;
                                                sprintBacklogData.put('sprint', model[0]).then(function (response) {

                                                }, function (error) {

                                                }).finally(function (ex) {

                                                });
                                            }
                                        }
                                        catch (ex) {
                                            console.log(ex);
                                        }
                                    }
                                    else {
                                        $scope.InProgressingloading = false;
                                    }


                                    ///// Calculate duration
                                    //getDatesBetweenTwoDates(model[0].start_date, model[0].end_date);                               
                                }, true);
                                /*
                                 * Sprint In Backlog
                                 */
                                $scope.$watch("sprintBackLog", function (model) {

                                    if ($scope.sprintBackLog.length > 0)
                                    {
                                        $scope.BackLogloading = true;
                                        for (var i = 0; i < $scope.sprintBackLog.length; i++)
                                        {
                                            if ($scope.sprintBackLog[i].status !== BACKLOG) {
                                                $scope.sprintBackLog[i].status = BACKLOG;
                                                $scope.sprintBackLog[i].isProgress = 0;

                                                sprintBacklogData.put('sprint', $scope.sprintBackLog[i]).then(function (response) {
                                                    if (response.data === 'error')
                                                    {
                                                        // Why error
                                                    }
                                                    else {
                                                    }
                                                    //if()
                                                }, function (response) {
                                                    // Handle Error somehow
                                                }).finally(function () {
                                                    $scope.InProgressingloading = false;
                                                });
                                            }
                                            // register event for each sprint
                                            $scope.$watch("sprintBackLog[" + i + "]", function (model) {
                                                try {
                                                    if (model.sprintId > 0)
                                                    {
                                                        //console.log()
                                                        if (model.userstories.length > 0) {
                                                            for (var i = 0; i < model.userstories.length; i++)
                                                            {
                                                                // Check if only one userstory have another sprintId ,
                                                                if (model.userstories[i].sprintId !== model.sprintId)
                                                                {
                                                                    model.userstories[i].sprintId = model.sprintId;
                                                                    productBacklogData.put('userstorys/' + model.userstories[i].userstoryId, model.userstories[i]).then(function (response) {

                                                                    }, function (error) {
                                                                        // Handle Error
                                                                    }).finally(function () {
                                                                        // 
                                                                    });
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            model.userstories = [];
                                                        }
                                                    }

                                                }
                                                catch (ex) {
                                                    //  console.log(ex);
                                                }

                                            });

                                            try {
                                                if ($scope.sprintBackLog[i].userstories)
                                                {
                                                    try {
                                                        var Point = calculatePoint($scope.sprintBackLog[i].userstories);
                                                        $scope.sprintBackLog[i].totalPoint = Point.totalPoint;
                                                        $scope.sprintBackLog[i].pointComplete = Point.completePoint;
                                                        sprintBacklogData.put('sprint', $scope.sprintBackLog[i]).then(function (response) {

                                                        }, function (error) {

                                                        }).finally(function (ex) {

                                                        });

                                                    }
                                                    catch (ex) {
                                                        console.log(ex);
                                                    }
                                                }
                                                else {
                                                    console.log($scope.sprintBackLog[i]);
                                                    $scope.sprintBackLog[i].userstories = [];

                                                    $scope.sprintBackLog[i].totalPoint = 0;
                                                    $scope.sprintBackLog[i].pointComplete = 0;
                                                    sprintBacklogData.put('sprint', $scope.sprintBackLog[i]).then(function (response) {

                                                    }, function (error) {

                                                    }).finally(function (ex) {
                                                        $scope.BackLogloading = false;
                                                    });
                                                }
                                            }
                                            catch (ex) {
                                                console.log(ex);
                                            }


                                        }
                                        $scope.BackLogloading = false;
                                    }
                                }, true);
                                //CalculateTotalPoint($scope,6);

                                // $scope.$watch()      
                                DrawBurnDownChart($scope);
                            });
                        }
                    })
                    .finally(function () {
                        $scope.loadingSprintPage = false;
                    });


            $scope.open = function (sprint) {
                sprint.projectId = $routeParams.projectId;
                var modalInstance = $modal.open({
                    templateUrl: 'sprint/sprintCreate.html',
                    controller: 'sprintCreateCtrl',
                    size: 'lg',
                    resolve: {
                        item: function () {
                            return sprint;
                        }
                    }
                });

                modalInstance.result.then(function (selectedObject) {
                    if (selectedObject.save === "insert") {
                        alert("vao day");
                        $scope.insertSprint(selectedObject);
                        $scope.sprints = $filter('orderBy')($scope.sprints, 'id', 'reverse');
                    } else if (selectedObject.save === "update") {
                            $scope.updateSprint(sprint,selectedObject );
                    }
                });

            };

            $scope.insertSprint = function (selectedObject) {
                selectedObject.dataType = "sprint";
                selectedObject.pointComplete = 0;
                selectedObject.totalPoint = 0;
                selectedObject.isMove = 0;
                selectedObject.isLock = 0;
                selectedObject.userstories = [];
                $scope.sprints.push(selectedObject);
                $scope.sprintBackLog.push(selectedObject);
                                console.log($scope.sprintBackLog);
            };

            $scope.updateSprint = function (sprint, selectedObject) {
                sprint.description = selectedObject.description;
                sprint.totalPoint = selectedObject.totalPoint;
                sprint.sprintName = selectedObject.sprintName;
                sprint.status = selectedObject.status;
                sprint.start_date = (selectedObject.start_date);
                sprint.end_date = (selectedObject.end_date);
                sprint.pointComplete = selectedObject.pointComplete;
                sprint.isBlock = selectedObject.isBlock;
            };
            // GET ALL US :D 
            $scope.openUserstory = function (userstory) {
                var modalInstance;
                if (userstory.status !== "DONE") {
                    modalInstance = $modal.open({
                        templateUrl: 'milestone/createNewUserstory.html',
                        controller: 'userStoryEditCtrl',
                        size: 'lg',
                        resolve: {
                            item: function () {
                                return userstory;
                            },
                            userstories: function () {
                                return userstories;
                            }
                        }
                    });
                } else {
                    modalInstance = $modal.open({
                        templateUrl: 'milestone/doneUserStory.html',
                        controller: 'userStoryEditCtrl',
                        size: 'lg',
                        resolve: {
                            item: function () {
                                return userstory;
                            },
                            userstories: function () {
                                return userstories;
                            }
                        }
                    });
                }

                modalInstance.result.then(function (selectedObject) {
                    if (selectedObject.save === "insert") {
                        $scope.userstories.push(selectedObject);

                        $scope.userstories = $filter('orderBy')($scope.userstories, 'id', 'reverse');
                    } else if (selectedObject.save === "update") {
                        userstory.point = selectedObject.point;
                        userstory.status = selectedObject.status;

                        // STOP here to calculate

                    }
                });
            };
        })
        .controller('sprintCreateCtrl', function ($scope, $modalInstance, item, sprintBacklogData) {

            $scope.sprint = angular.copy(item);
            $scope.title = ($scope.sprint.sprintId > 0) ? 'Edit Sprint' : 'Add new sprint';
            $scope.showArchieve = ($scope.sprint.sprintId > 0) ? true : false;
            $scope.model = ($scope.sprint.status === DONE) ? 'fa-archive' : 'fa-trash';
            $scope.Message = ($scope.sprint.status === DONE) ? 'Are you sure to archieve this sprint, In case this sprint is archieved, all the userstories belong will me archieve too' : 'Are you sure to delete this spint, In case this sprint is deleted, all user stories belong will be return to product Backlog';
            $scope.isCollapsed = true;
            $scope.buttonText = ($scope.sprint.sprintId > 0) ? 'Update Sprint' : 'Add New sprint';
            $scope.messageError = {
                condition: false,
                context: ""
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('Close');
            };
            /*
             * WATCH SPRINT.END_DATE
             */
            $scope.$watch("sprint.end_date", function (model) {
                if ($scope.sprint.end_date !== "" && $scope.sprint.start_date) {
                    if ($scope.sprint.end_date < $scope.sprint.start_date)
                    {
                        /// ben phia end
                        $scope.messageError.condition = true;
                        $scope.messageError.context = "End Date must be greater than Start Date";
                    }
                    else {
                        $scope.messageError.condition = false;
                    }
                }

            });
            /*
             * WATCH START DATE
             */
            $scope.$watch("sprint.start_date", function (model) {
                if ($scope.sprint.start_date !== "" && $scope.sprint.start_date) {
                    if ($scope.sprint.end_date < $scope.sprint.start_date)
                    {
                        // ben phiA START
                        $scope.messageError.condition = true;
                        $scope.messageError.context = "Start Date must be less than End Date";
                    }
                    else {
                        $scope.messageError.condition = false;
                    }
                }

            });
            $scope.saveSprint = function (sprint) {
                if (sprint.sprintId > 0) {
                    /// check if sprint.status = DONE
                    if (sprint.status === DONE) {
                        sprint.isBlock = 1;
                    }
                    else {
                        sprint.isBlock = 0;
                    }
                    //console.log(sprint);
                    var startDate = $("#startDate").val();
                    var endDate = $("#endDate").val();
                    sprint.start_date = startDate;
                    sprint.end_date = endDate;
                    sprintBacklogData.put('sprints/' + sprint.sprintId, sprint).then(function (result) {
                        if (result.data !== 'error') {
                            var x = angular.copy(sprint);
                            x.userstories = [];
                            x.save = 'update';
                            $modalInstance.close(x);
                        } else {
                            console.log(result);
                        }
                    });
                }
                else {
                    sprint.status = "BACKLOG";
                    sprint.isProgress = false;
                    var startDate = $("#startDate").val();
                    var endDate = $("#endDate").val();
                    sprint.start_date = startDate;
                    sprint.end_date = endDate;
                    sprint.userstories = [];
                    //alert(testDate);
                    sprintBacklogData.post('sprints', sprint).then(function (result) {
                        if (result.data !== 'error') {
                            var x = angular.copy(sprint);
                            x.sprintId = result.data;
                            x.save = 'insert';
                            $modalInstance.close(x);
                        }
                        else if (result.data === "duplicate") {
                            //  ("this project already have that Userstory name, ");
                        }
                        else {
                            // ("Can not add sprint because, I don't know");
                        }
                    });
                }
            };
            // Delete sprint function
            $scope.Delete = function (sprint) {
                /// If done Archieve
                if (sprint.status === DONE) {
                    $scope.ArchieveSprint(sprint);
                }
                else {
                    // Delete
                    $scope.DeleteSprint(sprint);
                }
                location.reload();
            };

            $scope.DeleteSprint = function (sprint) {
                sprintBacklogData.delete('sprint', sprint.sprintId).then(function (data) {
                    var tostring = data.data.toString();
                    if (JSON.stringify(tostring) !== JSON.stringify("error")) {

                        // close 
                        $modalInstance.dismiss('Close');
                    }
                    else {
                        // can not delete
                    }
                });
            };
            $scope.ArchieveSprint = function (sprint) {
                sprint.isAchieve = 1;
                sprintBacklogData.put('sprint', sprint).then(function (data) {
                    var tostring = data.data.toString();
                    if (JSON.stringify(tostring) !== JSON.stringify("error")) {
                        $modalInstance.dismiss('Close');
                    }
                    else {

                    }
                });
            };
            $scope.today = function () {
                $scope.dt = new Date();
            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };
            $scope.disabled = function (date, mode) {
                return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
            };
            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);

            $scope.events =
                    [
                        {
                            date: tomorrow,
                            status: 'full'
                        },
                        {
                            date: afterTomorrow,
                            status: 'partially'
                        }
                    ];


            $scope.Startopen = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.StartDateopened = true;
            };

            $scope.Endopen = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.EndDateopened = true;
            };

        })
        //// SCRUM BOARD HANDLE
        .controller("scrumBoardCtrl", function ($scope, $modal, $filter, $location, $routeParams, ScrumBoardData, productBacklogData, sprintBacklogData, socket, shareMemberInProject, sharedUserId, Data) {

            // Project ID
            $scope.sprintBoard = {};
            // Columns
            $scope.sprintBoard.columns = [""];
            $scope.sprintBackLog = [];
            // Classified depend on Status of sprint In PROGRESSING
            $scope.sprintInProgress = [];
            // Classified depend on Status of sprint DONE
            $scope.sprintDone = [];
            /// FIRST//
            $scope.columnInsert = {};
            $scope.newTask = {};
            $scope.orderByField = 'Priority';
            $scope.reverseSort = false;
            /*
             * get ScrumBoard Column By Default
             */
            $scope.columnInsert.projectId = $routeParams.id;
            // Test Service
            if (parseInt($routeParams.id) > 0) {
                var projectId = $routeParams.id;
                $scope.columnInsert.projectId = projectId;
                ///
                //
                var UserId = sharedUserId.getuserId();

                /// GET User in that CBoard

                var projectIdandUserId = {
                    "projectId": projectId,
                    "userId": -1
                };
                /// Hot hang no
                Data.getProjectByProjectID('project', projectIdandUserId.projectId).then(function (result) {
                    try {
                        //var checked = JSON.parse(result.data);
                        $scope.projectInformation = result.data[0];
                    }
                    catch (ex) {
                        console.log();
                    }
                });
                /*
                 * ALREADY HAVE USERID
                 */
                if (projectIdandUserId.userId < 1) {
                    // call ajax
                    if (user.username === "") {
                        user.username = getCookie("loginUser");
                    }
                    /*
                     * CALL AJAX GET USERID by USERNAME (unique)
                     */
                    Data.getUserId('user', user).then(function (data) {
                        // GET USER ID
                        //if()
                        projectIdandUserId.userId = data.data[0][0];
                        sharedUserId.setuserId(data.data[0]);
                        $scope.project = angular.copy(projectIdandUserId);
                    }).finally(function () {
                        //load Scrum Board
                        $scope.GetAllScrumBoardByProjectId(projectIdandUserId);
                    });
                }
                else {
                    $scope.project = angular.copy(projectIdandUserId);
                    ScrumBoardData.getAllScrumBoardByProjectId('scrumboard', projectIdandUserId)
                            .then(function (response) {
                                /// Scrum Board Column
                                //$scope.ScrumBoardColumn = response.data;
                                var tostring = response.data.toString();
                                /*/// Get Authentication from Server
                                 * 
                                 *
                                 *
                                 *IF no User with that project Id , server response notfoundproject*/
                                if (JSON.stringify(tostring) === JSON.stringify("notfoundproject")) {
                                    return $location.path('/notfound');
                                }
                                // If Server reponse with empty Us
                                else if (JSON.stringify(tostring) === JSON.stringify("nodata")) {
                                    $scope.sprints = [];
                                }
                                /// Else everything OK
                                else {
                                    try {
                                        if (response.data.length > 0)
                                        {
                                            console.log(response.data);
                                            $scope.ScrumBoardColumn = response.data;
                                        }
                                        else {
                                            ///Handle
                                            $scope.ScrumBoardColumn = [];
                                        }
                                    }
                                    catch (ex) {
                                    }
                                    return sprintBacklogData.getSprintByProjectId('sprint', projectIdandUserId);
                                }

                            })
                            .then(function (Sprintresponse) {
                                // Get SPRINT truoc
                                $scope.sprints = Sprintresponse.data;
                                return productBacklogData.getUserstoryByProjectId('userstory', projectIdandUserId);
                            })
                            .then(function (response) {
                                // EveryThing is in here
                                $scope.userstories = response.data;
                                // console.log($scope.userstories);
                                if ($scope.userstories.length > 0) {
                                    for (var i = 0; i < $scope.userstories.length; i++) {
                                        if (parseInt($scope.userstories[i].point) > 3) {
                                            $scope.userstories[i].height = parseInt($scope.userstories[i].point) * 120;
                                        }
                                        else if (parseInt($scope.userstories[i].point) > 0 && parseInt($scope.userstories[i].point) <= 3) {
                                            $scope.userstories[i].height = parseInt($scope.userstories[i].point) * 200;
                                        }
                                        else {
                                            $scope.userstories[i].height = 200;
                                            //     alert("a");
                                        }


                                        $scope.userstories[i].tasks = [];
                                    }
                                }

                                /// arragne
                                ArrageUserstory($scope);
                                /// BY default
                                $scope.sprintChoose = $scope.sprintInProgress[0];
                                /// Load Sprint
                                $scope.LoadSprint();
                                // get Members
                                $scope.userMembers = shareMemberInProject.getUserMemberAndRole();
                                if ($scope.userMembers.length > 0) {
                                    console.log($scope.userMembers);
                                }
                                else {
                                    $scope.LoadMembers();
                                }

                            });
                }
            }
            else {
                return $location.path('/notfound');
            }

            ///AJAX TO GET SCRUM BOARD BY PROJECTID
            $scope.GetAllScrumBoardByProjectId = function (projectIdandUserId) {
                ScrumBoardData.getAllScrumBoardByProjectId('scrumboard', projectIdandUserId)
                        .then(function (response) {
                            /// Scrum Board Column
                            //$scope.ScrumBoardColumn = response.data;
                            var tostring = response.data.toString();

                            /*/// Get Authentication from Server
                             * 
                             *
                             *
                             *IF no User with that project Id , server response notfoundproject*/
                            if (JSON.stringify(tostring) === JSON.stringify("notfoundproject")) {
                                return $location.path('/notfound');
                            }
                            // If Server reponse with empty Us
                            else if (tostring === JSON.stringify("nodata")) {
                                $scope.sprints = [];
                            }
                            /// Else everything OK
                            else {
                                try {
                                    // Load Project First                                   
                                    $scope.LoadProjects(projectIdandUserId);
                                    if (response.data.length > 0)
                                    {
                                        $scope.ScrumBoardColumn = response.data;
                                    }
                                    else {
                                        ///Handle
                                    }
                                }
                                catch (ex) {
                                }
                                return sprintBacklogData.getSprintByProjectId('sprint', projectIdandUserId);
                            }
                        })
                        .then(function (Sprintresponse) {
                            // Get SPRINT truoc
                            $scope.sprints = Sprintresponse.data;
                            return productBacklogData.getUserstoryByProjectId('userstory', projectIdandUserId);
                        })
                        .then(function (response) {
                            // EveryThing is in here
                            $scope.userstories = response.data;
                            $scope.productbacklogUserstory = response.data;
                            if ($scope.userstories.length > 0) {
                                for (var i = 0; i < $scope.userstories.length; i++) {
                                    if (parseInt($scope.userstories[i].point) > 3) {
                                        $scope.userstories[i].height = parseInt($scope.userstories[i].point) * 120;
                                    }
                                    else if (parseInt($scope.userstories[i].point) > 0 && parseInt($scope.userstories[i].point) <= 3) {
                                        $scope.userstories[i].height = parseInt($scope.userstories[i].point) * 200;
                                    }
                                    else {
                                        $scope.userstories[i].height = 200;
                                        // alert("a");
                                    }

                                    $scope.userstories[i].tasks = [];
                                }
                            }
                            //else if() 
                            /// arragne
                            /// sap xep lai user story
                            ArrageUserstory($scope);
                            /// BY default
                            $scope.sprintChoose = $scope.sprintInProgress[0];
                            /// Load Sprint
                            $scope.LoadSprint();
                            // get Members
                            $scope.userMembers = shareMemberInProject.getUserMemberAndRole();
                            if ($scope.userMembers.length > 0) {
                                console.log($scope.userMembers);
                            }
                            else {
                                $scope.LoadMembers();
                            }
                            /// 


                        });
            };
            // LOAD US AND SPRINT
            $scope.LoadSprint = function () {
                //console.log($scope.sprintChoose);
                if ($scope.sprintChoose) {
                    /// checking 
                    $scope.noDataFound = false;
                    $scope.noDataUserStoryFound = false;
                    // IF this is the sprint is Blocked
                    //console.log($scope.sprintChoose.isLock);
                    if ($scope.sprintChoose.isLock === 1 || $scope.sprintChoose.isLock === "1") {
                        /// pop - up show
                        $scope.sprintChoose.message = "Block"; /// show message block
                        var modalInstance = $modal.open({
                            templateUrl: 'scrumboardcolumn/message.html',
                            controller: "MessageCtrl",
                            size: 'sm',
                            resolve: {
                                itemSprint: function () {
                                    return $scope.sprintChoose;
                                }
                            }
                        });
                        modalInstance.result.then(function (selectedObject) {
                            console.log(selectedObject);
                            if (selectedObject === "ok") {
                                /// UN Block Sprint
                                window.location.href = "#/sprint/" + $routeParams.id;
                            }
                        });
                    }
                    /// NOT blocked
                    else {

                    }
                    if ($scope.sprintChoose.sprintId < 0) {
                        // handle
                        console.log($scope.sprintChoose);
                    }
                    else {
                        $scope.sprintChoose.userstories = getUserstoriesBySprintId($scope.userstories, $scope.sprintChoose.sprintId);
                        if ($scope.sprintChoose.userstories.length > 0) {
                            /// Load UserStories and asks
                            $scope.LoadUserStoried();

                        }
                        else {
                            //$scope.noDataFound = true;
                            $scope.noDataUserStoryFound = true;
                        }
                    }
                }
                else {
                    //"no sprint in that moment");
                    $scope.sprintChoose = [];
                    $scope.sprintChoose.userstories = [];
                    $scope.noDataFound = true;
                }
            };
            ///LOAD USERSTORIES
            $scope.LoadUserStoried = function () {
                for (var i in $scope.sprintChoose.userstories) {
                    (function (i) {
                        var userStoryId = $scope.sprintChoose.userstories[i].userStoryId;
                        $scope.sprintChoose.userstories[i].userStoryType = [userStoryId];
                        ScrumBoardData.getAllTaskBelongSprintWithUserStoryId("tasks", userStoryId).then(function (Taskresponse) {
                            $scope.sprintChoose.userstories[i].tasks = [];
                            $scope.sprintChoose.userstories[i].tasks = Taskresponse.data;
                            if ($scope.sprintChoose.userstories[i].tasks.length > 0) {
                                ($scope.GetTeamMeberAssignToTask(i));
                                //console.log(i);
                            }
                        }).finally(function () {
                            if ((parseInt(i) + 1) === $scope.sprintChoose.userstories.length) {
                                // xong :D
                                $scope.CalculateTime();
                            }
                        });
                    })(i);

                }

            };
            // Load member assigned to task
            $scope.GetTeamMeberAssignToTask = function (index) {
                for (var i in $scope.sprintChoose.userstories[index].tasks) {
                    (function (i) {
                        //console.log($scope.sprintChoose.userstories[index].tasks[i].taskId);          
                        ScrumBoardData.getUserMemberBelongToTaskId('task', $scope.sprintChoose.userstories[index].tasks[i].taskId).then(function (taskresponse) {
                            //var data = JSON.parse(taskresponse.data);                             //console.log(data);
                            if (taskresponse.data.length > 0) {
                                $scope.sprintChoose.userstories[index].tasks[i].assignedusers = taskresponse.data;
                            }
                            else {
                                $scope.sprintChoose.userstories[index].tasks[i].assignedusers = [];
                            }
                        });
                    })(i);
                }
            };
            // Load Members in project
            $scope.LoadMembers = function () {
                if (parseInt(projectIdandUserId.projectId) < 0 || parseInt(projectIdandUserId.userId) < 0) {
                    // get userid by username
                    //var username =
                    //Data.getUserId('user', ).then();                 }
                    $scope.GetAlluserInProject(projectIdandUserId);

                }
            };
            // Get all user member in project
            $scope.GetAlluserInProject = function (projectIdandUserId) {
                Data.getAllUserInProject('project', projectIdandUserId).then(function (result) {
                    if (result.data !== "error") {
                        $scope.userMembers = result.data;
                        shareMemberInProject.setUserMemberAndRole($scope.userMembers);
                    }
                });
            };
            /*
             * 
             * @param {type} array userstories
             * @returns {userstory.progress}
             */
            $scope.CalculateProgressOfUserStory = function (array) {
                var totalHour = 0, doneHour = 0;
                if (array.length > 0) {
                    for (var index in array) {
                        if (parseInt(array[index].isArchive) !== 1 || array[index].isArchive !== "1") {
                            totalHour += parseInt(array[index].estimateTime);
                            if (array[index].status === DONE) {
                                doneHour += parseInt(array[index].estimateTime);
                            }
                        }

                    }
                }
                var result = (doneHour / totalHour * 100).toFixed(2);
                return result;
            };
            /// ADD rask
            $scope.addTask = function (userstory, isLock, scrumBoardColumn) {
                var modalInstance;
                if (userstory.userStoryId > 0) {
                    modalInstance = $modal.open({
                        templateUrl: 'task/createNewTask.html',
                        controller: 'TaskCtrl',
                        size: 'lg',
                        resolve: {
                            item: function () {
                                return userstory;
                            },
                            isLock: function () {
                                return isLock;
                            },
                            scrumBoardColumns: function () {
                                return scrumBoardColumn;
                            }
                        }
                    });
                }

                modalInstance.result.then(function (selectedObject) {
                    if (selectedObject.save === "insert") {
                        var newTask = angular.copy(selectedObject);
                        //console.log(newTask);
                        userstory.tasks.push(selectedObject);

                        if ($scope.sprintChoose.userstories.length > 0) {
                            for (var i = 0; i < $scope.sprintChoose.userstories.length; i++) {
                                if ($scope.sprintChoose.userstories[i].userStoryId === selectedObject.userStoryId) {
                                    var array = angular.copy($scope.sprintChoose.userstories[i].tasks);
                                    var progress = $scope.CalculateProgressOfUserStory(array);
                                    if (progress > 0) {
                                        $scope.sprintChoose.userstories[i].progress = progress;
                                        /// update userstory
                                        $scope.UpdateUserstory($scope.sprintChoose.userstories[i]);
                                    }
                                    break;
                                }
                            }
                        }

                        $scope.CalculateTime();
                    }
                });
            };
            /// Update userstory
            $scope.UpdateUserstory = function (userstory) {
                /// if update success, return true
                productBacklogData.put('userstory', userstory).then(function (data) {
                    if (data.data !== "error") {
                        socket.emit('UpdateUserStory', userstory);
                        return true;
                    }
                    return false;
                });
                //else return false
            };
            // Edit task
            $scope.editTask = function (task, sprintChoose) {
                var modalInstance;
                if ($routeParams.id > 0) {
                    task.projectId = $routeParams.id;
                }
                if (task.taskId > 0) {
                    modalInstance = $modal.open({
                        templateUrl: 'task/detailTask.html',
                        controller: 'TaskEditCtrl',
                        size: 'lg',
                        resolve: {item: function () {
                                return task;
                            },
                            sprintChoose: function () {
                                return sprintChoose;
                            }
                        }
                    });
                }
                modalInstance.result.then(function (selectedObject) {
                    if (selectedObject.save === "update") {
                        task.taskName = selectedObject.taskName;
                        task.Description = selectedObject.Description;
                        task.ordinaryPosition = selectedObject.ordinaryPosition;
                        task.priority = selectedObject.priority;
                        task.projectId = selectedObject.projectId;
                        task.remaining = selectedObject.remaining;
                        task.scrumBoardColumnId = selectedObject.scrumBoardColumnId;
                        task.spentTime = selectedObject.spentTime;
                        task.status = selectedObject.status;
                        task.type = selectedObject.type;
                        task.userStoryId = selectedObject.userStoryId;
                        task.estimateTime = selectedObject.estimateTime;
                        task.isArchive = selectedObject.isArchive;
                        task.assignedusers = selectedObject.assignedusers;
                        socket.emit('updateTask', task);

                        /// update userstory's progress
                        $scope.UpdateProgressUserstory(task);
                        // Calcualate TIME
                        $scope.CalculateTime();
                    }
                    else {

                    }
                });

            };
            // Update ProgressUserstory
            $scope.UpdateProgressUserstory = function (task) {
                for (var index in $scope.sprintChoose.userstories) {
                    if ($scope.sprintChoose.userstories[index].userStoryId === task.userStoryId) {
                        var array = angular.copy($scope.sprintChoose.userstories[index].tasks)
                        var progress = $scope.CalculateProgressOfUserStory(array);
                        if (progress >= 0) {
                            $scope.sprintChoose.userstories[index].progress = progress;
                            var updateUS = angular.copy($scope.sprintChoose.userstories[index]);
                            /// update Userstory
                            $scope.UpdateUserstory(updateUS);
                        }
                    }
                }
            };

            // Calculate EstimateTime
            // SpentTime
            // RemainingTime
            $scope.CalculateTime = function () {
                //console.log($scope.sprintChoose);
                $scope.timebox = $scope.CalculateAllTime();
                //console.log($scope.timebox);
                if ($scope.timebox.estimateTime >= $scope.timebox.spentTime) {
                    $scope.timebox.remainingTime = $scope.timebox.estimateTime - $scope.timebox.spentTime;
                }
                else {
                    //$scope.timebox.estimateTime = $scope.timebox.spentTime;
                    $scope.timebox.remainingTime = 0;
                }
                //  console.log( $scope.timebox);
            };
            $scope.CalculateAllTime = function () {
                // console.log($scope.sprintChoose);
                var timebox = {
                    spentTime: 0,
                    estimateTime: 0,
                    remainingTime: 0
                };
                if ($scope.sprintChoose.userstories.length > 0) {
                    for (var indexUS in $scope.sprintChoose.userstories) {
                        //console.log($scope.sprintChoose.userstories[indexUS].name);
                        //  console.log($scope.sprintChoose.userstories[indexUS].tasks[indexUS]);
                        for (var indexT in $scope.sprintChoose.userstories[indexUS].tasks) {
                            timebox.spentTime += parseInt($scope.sprintChoose.userstories[indexUS].tasks[indexT].spentTime);
                            //console.log(sprint.userstories[indexUS].tasks[indexT]);
                            //  alert(indexT);
                            timebox.estimateTime += parseInt($scope.sprintChoose.userstories[indexUS].tasks[indexT].estimateTime);
                        }
                    }
                }
                return timebox;
            };
            /// Realtime
            /// In comming Manage task
            //// someone in localhost Add new task
            socket.on('onTaskCreated', function (newTask) {
                //console.log("co nguoi tao");
                //console.log(newTask);
                ///$scope.notes.push(data);

                //// FIND 
                if ($scope.sprintChoose.userstories.length > 0) {
                    for (var i = 0; i < $scope.sprintChoose.userstories.length; i++) {
                        if ($scope.sprintChoose.userstories[i].userStoryId === newTask.userStoryId) {
                            $scope.sprintChoose.userstories[i].tasks.push(newTask);
                        }
                    }
                }
            });
            /// Update Task
            socket.on('onTaskUpdated', function (updatedTask) {
                //console.log(updatedTask);                 if (updatedTask.taskId > 0) {
                for (var index in $scope.sprintChoose.userstories) {
                    for (var indexj in $scope.sprintChoose.userstories[index].tasks) {
                        if ($scope.sprintChoose.userstories[index].tasks[indexj].taskId === updatedTask.taskId) {
                            $scope.sprintChoose.userstories[index].tasks[indexj] = (updatedTask);
                            $scope.CalculateTime();
                            var array = angular.copy($scope.sprintChoose.userstories[index].tasks);
                            var progress = $scope.CalculateProgressOfUserStory(array);
                            if (progress >= 0) {
                                $scope.sprintChoose.userstories[index].progress = progress;
                                var updateUS = angular.copy($scope.sprintChoose.userstories[index]);
                                // update USerstory HERE
                                productBacklogData.put('userstorys/' + updateUS.userstoryId, updateUS).then(function (response) {
                                    if (response.data !== "error") {
                                        socket.emit('UpdateUserStory', updateUS);
                                    }
                                    else {
                                        alert("can not update user story");
                                    }
                                });

                            }
                            break;
                        }

                    }
                }
            });
            /// Delete
            socket.on('onTaskDeleted', function (task) {
                // $scope.handleDeletedNoted(task);

            });
            ///sort task 
            socket.on('onSortTask', function (data) {
                console.log(data);
                try {
                    console.log(data);
                    var arrayTaskWithScrumBoardCoumnId = [];
                    console.log(arrayTaskWithScrumBoardCoumnId);
                    // find 
                    for (var i in $scope.sprintChoose.userstories) {
                        if (data.userStoryId === $scope.sprintChoose.userstories[i].userStoryId) {
                            // how to swap two task?
                            var j = 0;
                            for (var ii in $scope.sprintChoose.userstories[i].tasks) {
                                if (($scope.sprintChoose.userstories[i].tasks[ii].taskId === data.oldtaskid) || ($scope.sprintChoose.userstories[i].tasks[ii].taskId === data.newtaskId)) {
                                    arrayTaskWithScrumBoardCoumnId[j] = ii;
                                    j++;
                                }
                            }
                            // swap  
                            var object1 = $scope.sprintChoose.userstories[i].tasks[arrayTaskWithScrumBoardCoumnId[0]];
                            var object2 = $scope.sprintChoose.userstories[i].tasks[arrayTaskWithScrumBoardCoumnId[1]];
                            console.log(object1);
                            console.log(object2);

                            $scope.sprintChoose.userstories[i].tasks[arrayTaskWithScrumBoardCoumnId[1]] = object1;
                            $scope.sprintChoose.userstories[i].tasks[arrayTaskWithScrumBoardCoumnId[0]] = object2;
                            console.log($scope.sprintChoose.userstories[i].tasks);
                        }
                    }
                    console.log(arrayTaskWithScrumBoardCoumnId);
                }
                catch (ex) {
                    console.log(ex);
                }
            });
            //// Manage Column
            socket.on('onCreatNewColumn', function (newColumn) {
                //console.log("co nguoi tao");
                ////FIND
                if (newColumn.scrumBoardColumnId > 0) {
                    $scope.ScrumBoardColumn.push(newColumn);
                    //console.log("o so ke");
                }
            });
            ///InComming Manage UserStory
            socket.on('onUpdatedUserStory', function (updateUs) {
                //console.log("co nguoi update US");
                /// find it for me
                for (var index  in $scope.sprintChoose.userstories) {
                    if ($scope.sprintChoose.userstories[index].userStoryId === updateUs.userStoryId) {
                        $scope.sprintChoose.userstories[index].progress = updateUs.progress;
                        break;
                    }
                }
            });
            // Incoming Column Manage
            socket.on('onArchiveColumn', function (archivedScolumn) {
                for (var index in $scope.ScrumBoardColumn) {
                    console.log($scope.ScrumBoardColumn[index].scrumBoardColumnId + " vs " + archivedScolumn.scrumBoardColumnId);
                    if ($scope.ScrumBoardColumn[index].scrumBoardColumnId === archivedScolumn.scrumBoardColumnId)
                    {
                        $scope.ScrumBoardColumn[index] = archivedScolumn;
                        break;
                    }
                }
            });
            //// SubmitSprint
            socket.on('onSubmitSprint', function (sprintChoose) {
                $scope.sprintChoose = [];
                $scope.sprintChoose.userstories = [];
                $scope.noDataFound = true;
            });

            /*
             * This is drag over Call back
             */
            $scope.dragoverCallback = function (event, index, external, type) {
                $scope.logListEvent("dragged over: ", event, index, external, type);
                return index > 0;
            };
            /*
             * This is drop call back event when some task drop
             */
            $scope.dropCallback = function (event, index, item, external, type, allowedType, status, tasks) {
                $scope.handleEventMove(item, index, external, status, tasks);
                if (external) {
                    if (allowedType === 'itemType' && !item.label)
                        return false;
                    if (allowedType === 'containerType' && !angular.isArray(item))
                        return false;
                }
                return item;
            };
            /*
             * Log event              */
            $scope.logEvent = function (message, event) {
                //console.log(message, '(triggered by the following', event.type , 'event )');
                //console.log(event);
            };
            /*
             * Log List Event
             */
            $scope.logListEvent = function (action, event, index, external, type) {
                var message = external ? 'External ' : '';
                message += type + ' element is ' + action + ' position ' + index;
                $scope.logEvent(message, event);
            };
            /*
             */
            $scope.handleEventMove = function (task, index, external, status, tasks) {
                //console.log($scope.ScrumBoardColumn);
                ////// Find scrumBoardColunmid by status
                //$filter
                var selectedColumn, scrumBoardId;
                try {
                    var found = $filter('filter')($scope.ScrumBoardColumn, {name: status}, true);
                    if (found.length) {
                        selectedColumn = angular.copy(found[0]);
                        scrumBoardId = (selectedColumn.scrumBoardColumnId);
                    }
                    else {
                        selectedColumn = "not found";
                    }

                }
                catch (ex) {
                    console.log(ex);
                }
                console.log(status);
                console.log(task);
                if (task.status === status) {
                    //// THIS IS SORTABLE
                    var arrayTask = [];
                    /// loc task ra va xem index
                    var jndex = 0;
                    if (tasks.length > 0) {
                        for (var i in tasks) {
                            if (tasks[i].status === status) {
                                tasks[i].index = jndex++;
                                arrayTask.push(tasks[i]);
                            }
                        }
                    }
                    if (arrayTask.length > 0) {
                        var oldIndex;
                        for (var i in arrayTask) {
                            if (arrayTask[i].taskId === task.taskId) {
                                oldIndex = i;
                                break;
                            }
                        }
                        for (var i in arrayTask) {
                            var temporary;
                            if (index === arrayTask[i].index)
                            {
                                temporary = arrayTask[i].odinaryPosition;
                                arrayTask[i].odinaryPosition = task.odinaryPosition;
                                task.odinaryPosition = temporary;
                                ScrumBoardData.updateTaskbyTaskId('scrumboard', task).then(function (response) {
                                    for (var ii in arrayTask) {
                                        if (arrayTask[ii].taskId === task.taskId) {
                                            arrayTask[ii].odinaryPosition = task.odinaryPosition;
                                        }
                                    }
                                });
                                ScrumBoardData.updateTaskbyTaskId('scrumboard', arrayTask[i]).then(function (response) {
                                    if (response.data !== 'error') {
                                        var data = {
                                            oldtaskid: task.taskId,
                                            newtaskId: arrayTask[i].taskId,
                                            userStoryId: task.userStoryId,
                                            scrumBoardColumnId: task.scrumBoardColumnId
                                        };
                                        if (data.oldtaskid !== data.newtaskId) {
                                            socket.emit('sortTask', data);
                                        }
                                        else {
                                        }
                                    }
                                });
                                break;
                                /// het vong lap                                
                            }
                        }
                    }
                }

                else {

                    // Get all task belong ScrumBoardId
                    //after that update again the odinary position 
                    if (scrumBoardId > 0) {
                        task.scrumBoardColumnId = scrumBoardId;
                        task.status = status;
                        //task.scrumBoardColumnName = status;
                        if (task.status === "DONE") {
                            task.spentTime = task.estimateTime;

                        }
                        else if (task.status === "TODO") {
                            task.spentTime = 0;
                        }
                        var ArrayTask;
                        var arrayObject = {
                            scrumBoardColumnId: task.scrumBoardColumnId,
                            userStoryId: task.userStoryId
                        }; //content scrumBoard Id and userStoryId

                        ScrumBoardData.getAllTaskByScrumBoardId('scrumboard', arrayObject).then(function (responseColumn) {
                            if (responseColumn.data !== 'error') {
                                ArrayTask = responseColumn.data;
                                //console.log(ArrayTask);
                                var temporary;
                                if (parseInt(index) >= ArrayTask.length) {
                                    //
                                    //alert("CLGT???");
                                    task.odinaryPosition = parseInt(index);
                                    $scope.UpdateTaskByTaskId(task);
                                    $scope.UpdateProgressUserstory(task);
                                } else {
                                    for (var i in ArrayTask) {
                                        if (parseInt(i) === parseInt(index)) {
                                            temporary = task.odinaryPosition;
                                            task.odinaryPosition = ArrayTask[i].odinaryPosition;
                                            ArrayTask[i].odinaryPosition = temporary;
                                            var updateTask = angular.copy(ArrayTask[i]);
                                            // update
                                            ScrumBoardData.updateTaskbyTaskId('scrumboard', updateTask).then(function (responseTask) {
                                                if (responseTask.data !== 'error') {
                                                    $scope.UpdateTaskByTaskId(task);
                                                    $scope.UpdateProgressUserstory(task);
                                                }
                                            });
                                            break;
                                        }
                                    }
                                }
                            }

                        });
                    }
                }
            };
            // UPDATE task
            $scope.UpdateTaskByTaskId = function (task) {
                ScrumBoardData.updateTaskbyTaskId('scrumboard', task).then(function (responseMoveTask) {
                    if (responseMoveTask.data !== 'error') {
                        //  alert("update task");
                        $scope.CalculateTime();
                        socket.emit('updateTask', task);
                        //UpdateUserstoryProgress
                    }
                });
            };
            /// update userstory after update task     
            /// set event
            /// Create new Column
            $scope.CreateColumn = function (columnInsert, ScrumBoardColumn) {
                var modalInstance;
                modalInstance = $modal.open({
                    templateUrl: 'scrumboardcolumn/addColumn.html',
                    controller: 'createScrumBoardColumnCtrl',
                    size: 'sm',
                    resolve: {
                        item: function () {
                            return columnInsert;
                        },
                        ScrumBoardColumn: function () {
                            return ScrumBoardColumn;
                        }
                    }
                });

                modalInstance.result.then(function (selectedObject) {
                    if (selectedObject.save === "insert") {
                        $scope.ScrumBoardColumn.push(selectedObject);
                    }
                });

            };
            /// Archive Column
            $scope.ArchiveThisColumn = function (columnArchive) {
                columnArchive.isArchive = 1;
                ScrumBoardData.updateScrumBoardColumn('scrumboard', columnArchive).then(function (response) {
                    $scope.ScrumBoardColumn = response.data;
                    $scope.SendSocket('ArchivedColumn', columnArchive);
                });
            };
            /// DELETE ALL TASK WITH STATUS BEASED ON COLUMN 
            $scope.CompleteThisSprint = function (currentSprint) {
                ///
                // Case1: Is all Userstories is already DONE?
                // 
                var isCurrentSprintCompletelyDONE = true, count = 0;
                if (currentSprint) {
                    if (currentSprint.userstories.length > 0) {
                        for (var index in currentSprint.userstories) {
                            if (parseInt(currentSprint.userstories[index].progress) < 100) {
                                count++;
                            }


                        }
                        if (count > 0) {
                            /// show pop - up warning
                            ///$scope.sprintChoose

                            currentSprint.message = "CompletedUnFinishedSprint";
                            var modalCompleteSprint = $modal.open({
                                templateUrl: 'scrumboardcolumn/message.html',
                                controller: 'MessageCtrl',
                                size: 'md',
                                resolve: {
                                    itemSprint: function () {
                                        return currentSprint;
                                    }
                                }
                            });

                            modalCompleteSprint.result.then(function (selectedObject) {
                                if (selectedObject === "ok") {
                                    for (var index in currentSprint.userstories) {
                                        if (currentSprint.userstories[index].progress < 100) {
                                            /// Go back to productBacklog with status TODO
                                            $scope.GoBackProductBacklog(currentSprint.userstories[index]);
                                        }
                                        else {
                                            currentSprint.userstories[index].status = DONE;
                                            $scope.UpdateUserstory(currentSprint.userstories[index]);
                                        }
                                    }
                                    $scope.UpdateThisSprintDone(currentSprint);
                                    ///
                                }
                                else {

                                }
                            });
                        }
                        else {
                            /// still pop-up warning but difference message
                            currentSprint.message = "CompleteFinishedSprint";
                            var modalCompleteSprint = $modal.open({
                                templateUrl: 'scrumboardcolumn/message.html',
                                controller: 'MessageCtrl',
                                size: 'md',
                                resolve: {
                                    itemSprint: function () {
                                        return currentSprint;
                                    }
                                }
                            });

                            modalCompleteSprint.result.then(function (selectedObject) {
                                if (selectedObject === "ok") {
                                    //COMPLETE SPRINT
                                    $scope.UpdateThisSprintDone(currentSprint);
                                    /// mark all userstories on this sprint to DONE 
                                    for (var index in currentSprint.userstories) {
                                        currentSprint.userstories[index].status = DONE;
                                        $scope.UpdateUserstory(currentSprint.userstories[index]);
                                    }
                                }
                                else {

                                }
                            });
                        }
                    }
                    else if (currentSprint.sprintId > 0) {
                        // still mark it done
                        $scope.UpdateThisSprintDone(currentSprint);
                    }
                    else {
                        currentSprint.message = "There are no sprint at this moment";
                        var modalWarningSprint = $modal.open({
                            templateUrl: 'scrumboardcolumn/message.html',
                            controller: 'MessageCtrl',
                            size: 'sm',
                            resolve: {
                                itemSprint: function () {
                                    return currentSprint;
                                }}
                        });

                        modalWarningSprint.result.then(function (objectReturn) {
                            if (objectReturn === "ok") {
                                // go back to sprint Page
                                //alert("go back");
                                window.location.href = "#/sprint/" + projectId;
                            }
                        });

                    }
                }
                else {
                    var currentSprint = [];
                    currentSprint.message = "NotFoundSprint";
                    var modalWarningSprint = $modal.open({
                        templateUrl: 'scrumboardcolumn/message.html',
                        controller: 'MessageCtrl',
                        size: 'sm',
                        resolve: {
                            itemSprint: function () {
                                return currentSprint;
                            }}
                    });

                    modalWarningSprint.result.then(function (objectReturn) {

                    });
                }
                // Case 2: still One Userstories not yet DONE
                //Case Sub2-1:
                /// There are the next planning Sprint
                // Case Sub2-2:
                /// Go back to Product Backlog
            };
            //Update this print to DONE
            $scope.UpdateThisSprintDone = function (currentSprint) {
                currentSprint.status = DONE;
                sprintBacklogData.put('sprints/' + currentSprint.sprintId, currentSprint).then(function (response) {
                    if (response.data !== "error") {
                        $scope.sprintChoose = [];
                        $scope.sprintChoose.userstories = [];
                        $scope.noDataFound = true;
                        $scope.SendSocket("SubmitSprint", $scope.sprintChoose);
                        socket.emit("SubmitSprint", $scope.sprintChoose);
                    }
                });
            };
            // Go BACk to product Backlog
            $scope.GoBackProductBacklog = function (userstory) {
                userstory.status = "TODO";
                userstory.sprintId = null;
                $scope.UpdateUserstory(userstory);
            };
            //Select All Tasks belong to sprint
            $scope.SeeSpecificSprint = function () {
                $scope.LoadSprint();
            };
            // SOCKET
            $scope.SendSocket = function (message, object) {
                socket.emit(message, object);
            };


            $scope.LoadProjects = function (user) {
                Data.get('projects', user).then(function (data) {
                    $scope.filteredProjects = data.data;
                });
            }
        })
        .controller("TaskCtrl", function ($scope, $modalInstance, $timeout, $filter, item, isLock, scrumBoardColumns, ScrumBoardData, socket, shareMemberInProject) {
            var messageTimer = false, displayDuration = 10000;
            $scope.shouldBeOpen = true;
            ///ScrumBoardColumn
            $scope.userstoryChoose = angular.copy(item);
            $scope.scrumBoardColumns = angular.copy(scrumBoardColumns);
            /// Find Column TODO to add new task
            /// try catch()
            var selectedColumn;
            try {
                var found = $filter('filter')($scope.scrumBoardColumns, {name: "TODO"}, true);
                if (found.length) {
                    selectedColumn = angular.copy(found[0]);
                }
                else {
                    selectedColumn = "not found";
                }
            }
            catch (ex) {

            }

            if (isLock) {
                $scope.isLock = isLock;
            }
            $scope.cancel = function () {
                $scope.shouldBeOpen = false;
                $modalInstance.dismiss('Close');
            };
            $scope.task = {};
            if (item.userStoryId > 0) {
                $scope.task.userStoryId = item.userStoryId;
            }             // members
            $scope.userMembers = shareMemberInProject.getUserMemberAndRole();
            if ($scope.userMembers.length > 0) {
            }
            else {
            }
            $scope.messageError = {condition: true,
                context: "Require task Name and estimate hours"
            };
            if (messageTimer) {
                $timeout.cancel(messageTimer);
            }
            messageTimer = $timeout(function () {
                $scope.messageError.condition = false;
            }, displayDuration);
            $scope.saveTask = function (task) {
                if (task.taskId > 0)
                {


                }
                else {
                    task.status = "TODO";
                    var columnTODOid = (selectedColumn.scrumBoardColumnId);
                    if (columnTODOid > 0) {
                        task.scrumBoardColumnId = columnTODOid;
                    }
                    else {
                        /// Error!!!!!!!
                    }
                    task.remaining = 0;
                    task.spentTime = 0;
                    ScrumBoardData.addTask("addtask", task).then(function (result) {
                        // Load                     
                        if (result.data !== 'error') {
                            console.log(result);
                            var taskInsert = angular.copy(task);
                            taskInsert.taskId = result.data[0];
                            taskInsert.odinaryPosition = result.data[1];
                            taskInsert.assignedusers = [];
                            taskInsert.save = 'insert';
                            socket.emit('createTask', taskInsert);
                            $modalInstance.close(taskInsert);
                        }
                        else if (result.data === "duplicate") {
                        }
                        else {
                        }
                        //item.tasks.push(task);
                    });
                }


            };

        })
        .controller("TaskEditCtrl", function ($scope, $modalInstance, $filter, item, sprintChoose, ScrumBoardData, shareMemberInProject, sharedUserId, socket) {
            /// PROJECT iD?
            $scope.task = angular.copy(item);
            //$scope.
            if (sprintChoose) {
                $scope.sprintChoose = angular.copy(sprintChoose);
            }
            // test get user member and role
            $scope.userMembers = shareMemberInProject.getUserMemberAndRole();

            $scope.isBlockMoveTask = sprintChoose.isLock;
            // console.log(sprintChoose.status);
            if ($scope.task.status === 'TODO') {
                $scope.selectWithOptionsIsVisible = false;
            }
            else {
                $scope.selectWithOptionsIsVisible = true;
            }

            // close button
            $scope.cancel = function () {
                $modalInstance.dismiss('Close');
            };

            $scope.ScrumBoardColumns = [];
            ////
            //// GET status, Or Scrum Board name
            ///
            if ($scope.task.projectId > 0) {
                //console.log(sharedUserId.getuserId()[0]);
                var projectIdAndUserId = {
                    projectId: $scope.task.projectId,
                    userId: sharedUserId.getuserId()[0]
                };
                ScrumBoardData.getAllScrumBoardByProjectId('scrumboard', projectIdAndUserId).then(function (data) {
                    //console.log($scope.colors);                     console.log(data.data);
                    if (data.data !== "") {
                        $scope.ScrumBoardColumns = data.data;
                    }
                }).finally(function () {
                    if ($scope.userMembers.length <= 0) {
                        //// Get all User in project
                        ScrumBoardData.getAllUserInProject('scrumboard', projectIdAndUserId).then(function (resultData) {
                            console.log(resultData);
                            $scope.userMembers = resultData.data;
                        });
                    }
                });
            }
            /// Assigned User to task
            $scope.toggleSelection = function (user) {
                console.log(user);
                console.log($scope.task.assignedusers);
                var foundItem = $filter('filter')($scope.task.assignedusers, {userId: user.userId}, true)[0];
                var idx = $scope.task.assignedusers.indexOf(foundItem);
                var object = {
                    taskId: $scope.task.taskId,
                    userId: user.userId
                };
                /// is currently selected
                if (idx > -1) {
                    /// call ajax to remove user 
                    ScrumBoardData.removeUserFromTask('scrumboard', object).then(function (response) {
                        if (response.data !== "error") {
                            $scope.task.assignedusers.splice(idx, 1);
                        }
                    });
                }

                // is newly selected
                else {
                    // call ajax to insert user to task
                    ScrumBoardData.addUserToTask('srumboard', object).then(function (response) {
                        if (response.data !== "error") {
                            $scope.task.assignedusers.push(user);
                        }
                    });
                }
            };

            $scope.checkTaskMemInUserList = function (user) {
                try {
                    var foundItem = $filter('filter')($scope.task.assignedusers, {userId: user.userId}, true)[0];
                    if (foundItem !== undefined)
                        return true;
                    return false;
                } catch (ex) {
                }

            };

            $scope.saveTask = function (task) {

                if ($scope.task.taskId > 0) {
                    /// Ajax                     //// update 
                    if (task.status === "DONE") {
                        task.spentTime = task.estimateTime;
                    }
                    else if (task.status === "TODO") {
                        task.spentTime = 0;
                    }
                    //// FInd Scrumboard Id
                    var selectedColumn;
                    try {
                        //console.log(task.status);
                        var found = $filter('filter')($scope.ScrumBoardColumns, {name: task.status}, true);
                        if (found.length) {
                            selectedColumn = angular.copy(found[0]);
                            task.scrumBoardColumnId = selectedColumn.scrumBoardColumnId;
                        }
                        else {
                            selectedColumn = "not found";
                        }
                    }
                    catch (ex) {
                        console.log(ex);
                    }
                    //console.log(task.scrumBoardColumnId);
                    ScrumBoardData.updateTaskbyTaskId('scrumboard', $scope.task).then(function (response) {
                        if (response.data !== "error") {
                            //console.log(response.data);
                            var x = angular.copy($scope.task);
                            x.save = 'update';
                            $modalInstance.close(x);
                        }
                    });
                }
            };
            $scope.Archive = function (task) {
                /// archive this task
                if (task.taskId) {
                    /////////////
                    task.isArchive = 1;
                    ScrumBoardData.deleteTaskbyTaskId('scrumboard', task).then(function (response) {
                        console.log(response.data);
                        if (response.data !== "error") {
                            var x = angular.copy(task);
                            x.save = 'update';
                            socket.emit('updateTask', task);
                            $modalInstance.close(x);
                        }
                    });
                }
            };


        })
        .controller("createScrumBoardColumnCtrl", function ($scope, $modalInstance, item, ScrumBoardColumn, ScrumBoardData, socket) {
            $scope.cancel = function () {
                $modalInstance.dismiss('Close');
            };

            $scope.scrumBoardColumns = {};
            $scope.scrumBoardColumns = angular.copy(ScrumBoardColumn);
            $scope.saveScrumColumn = function (column) {
                if (column.scrumBoardColumnId > 0) {
                    /// Update
                } else {
                    ///// Create
                    if (column.name) {
                        //column.projectId = $modalInstance.id;
                        column.projectId = item.projectId;
                        column.isCanDelete = 1;
                        ScrumBoardData.addScrumColumn('scrumboard', column).then(function (response) {
                            if (response.data !== "error")
                            {
                                // send socket
                                console.log(response.data);
                                var newColumn = angular.copy(column);
                                newColumn.save = "insert";
                                newColumn.scrumBoardColumnId = response.data.scrumBoardColumnId;
                                newColumn.positionColumn = response.data.positionColumn;
                                socket.emit('createColumn', newColumn);
                                $modalInstance.close(newColumn);
                            }
                        });
                    }
                }
            };
        })
        .controller("MessageCtrl", function ($scope, $modalInstance, itemSprint) {

            $scope.cancel = function () {
                $modalInstance.dismiss('Close');
            };
            if (itemSprint.message === "Block") {
                $scope.message = "This sprint is blocked, because the end date have been passed!. You need to Unblock before trying to use ScrumBoard!";
                $scope.buttonText = "UnBlock this sprint";
                $scope.request = "UnBlock";
            }
            else if (itemSprint.message === "CompletedUnFinishedSprint") {
                $scope.message = "Note: There are still userstory items in the sprint that are not yet done. If you complete the sprint now, those unfinished items will be unassigned from the sprint.They will keep their progress and you can continue to work on them by assigning them to another sprint later on. If you do that, all of the work will be credited to that sprint, not this one. \nTIP: if you want some of the work to be credited to this sprint, you can split the unfinished item first";
                $scope.buttonText = "Unassign the userstory and complete the sprint";
                $scope.request = "Unassign";
            }
            else if (itemSprint.message === "CompleteFinishedSprint") {
                $scope.message = "Do you want to complete " + itemSprint.sprintName;
                $scope.buttonText = "Sure";
                $scope.request = "Completed";
            }

            else {
                $scope.message = "There are no sprint at the moment, so that you can not submit this sprint function!!!";
                $scope.buttonText = "Go back to sprint page";
            }

            $scope.responseYesMessage = function (request) {
                console.log(request);
                if (request !== " ") {
                    $modalInstance.close("ok");
                }
            };
        });


/// manageUser
function userManage($scope, Data) {
    var username = getCookie("loginUser");
    var user = {
        username: username,
        userId: 0
    };
    Data.getuserByUserName('user', user).then(function (response) {
        if (response.data !== "notfound") {
            $scope.user = response.data;
        }
        else {
            //  alert("can not find");
        }
    });
}
function countProductOwner(array, role) {
    var countPO = 0;
    /// timeout
    for (var index in array) {
        if (array[index].roleName === role) {
            countPO++;
        }
    }
    return countPO;
}
/*
 * 
 * GLOBAL FUNCTION
 * 
 */
function getSprintNameForUS($scope) {
    for (var i = 0; i < $scope.userstories.length; i++)
    {
        for (var j = 0; j < $scope.sprintsOfProject.length; j++)
        {
            if ($scope.userstories[i].sprintId === $scope.sprintsOfProject[j].sprintId)
            {
                $scope.userstories[i].sprintName = $scope.sprintsOfProject[j].sprintName;
            }
        }
    }
}
function GetSprintNameById($scope, sprintId) {
    try {
        for (var i = 0; i < $scope.sprintsOfProject.length; i++)
        {
            if ($scope.sprintsOfProject[i].sprintId === sprintId)
            {
                return $scope.sprintsOfProject[i].sprintName;
            }
        }
    }
    catch (ex) {
        console.log();
    }
}
///Directive
// Set USerID
function setUserId(userId) {
    user.userId = userId;
}
function getUserId() {
    return user.userId;
}
function getAllUSName(array) {
    var allUSName = "";
    if (array.length > 0) {
        for (var i = 0; i < (array.length - 1); i++) {
            allUSName += (array[i].name).trim() + "~/~";
        }
        var index = array.length - 1;
        allUSName += (array[index].name).trim();
    }
    return allUSName;
}
function getAllTasksName(arrayTask) {
    var allUSName = "";
    if (arrayTask.length > 0) {
        for (var i = 0; i < (arrayTask.length - 1); i++) {
            allUSName += (arrayTask[i].taskName).toString().trim() + "~/~";
        }
        var index = arrayTask.length - 1;
        allUSName += (arrayTask[index].taskName).trim();
    }
    return allUSName;
}
function calculatePoint(userstories) {
    var array = {
        unschedule: 0,
        inschedule: 0,
        inProgress: 0,
        Rest: 0,
        accepted: 0,
        unacepted: 0,
        undev: 0
    };
    if (userstories.length > 0)
    {
        for (var i = 0; i < userstories.length; i++)
        {
            //console.log("Point " + parseInt(userstories[i].point));
            array.Rest += parseInt(userstories[i].point);
            ;
            if (userstories[i].status === "TODO") {
                array.unschedule += parseInt(userstories[i].point);
                ;
            }
            else if (userstories[i].status === "IN PROGRESSING") {
                array.inProgress += parseInt(userstories[i].point);
                ;
            }
            else if (userstories[i].status === "DONE") {
                array.accepted += parseInt(userstories[i].point);
                ;
            }
        }
        array.inschedule = array.Rest - array.unschedule;
        array.undev = array.Rest - array.inProgress;
        array.unacepted = array.Rest - array.accepted;
    }
    //console.log(array);
    return array;
}
function DrawChart(userstoryPoint) {
    console.log(userstoryPoint);
    var dataUnschedule = google.visualization.arrayToDataTable([
        ['user story', 'point per US'],
        ['UnSchedule ', userstoryPoint.unschedule],
        ['In Schedule', userstoryPoint.inschedule]
    ]);

    var optionsUnschedule = {
        title: 'Un Schedule Point',
        pieHole: 0.6
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutPoint'));
    chart.draw(dataUnschedule, optionsUnschedule);


    var dataInDev = google.visualization.arrayToDataTable([
        ['user story', 'point per US'],
        ['In Dev ', userstoryPoint.inProgress],
        ['Un Dev', userstoryPoint.undev]
    ]);

    var optionsInDev = {
        title: 'In Develop Point',
        pieHole: 0.6
    };

    var chartDev = new google.visualization.PieChart(document.getElementById('inDev'));
    chartDev.draw(dataInDev, optionsInDev);



    var dataAccepted = google.visualization.arrayToDataTable([
        ['user story', 'point per US'],
        ['Accepted ', userstoryPoint.accepted || 0],
        ['UnAcepted', userstoryPoint.unacepted || 0]
    ]);

    var optionsAccepted = {
        title: 'Accepted Point',
        pieHole: 0.6
    };

    var chartAccepted = new google.visualization.PieChart(document.getElementById('accepted'));
    chartAccepted.draw(dataAccepted, optionsAccepted);

}
function getShowDiv(array) {
    // the number of Table is Show in the specific time
    var num = 0;
    if (array.length > 0) {
        for (var i = 0; i < array.length; i++)
        {
            if (array[i].isShow === true)
            {
                num++;
            }
        }
    }
    return num;
}
function initialToggle($scope) {
    $scope.toggleProductBacklog = function () {
        $scope.currentProductBacklog = !$scope.currentProductBacklog;
        if ($scope.currentProductBacklog) {
            $scope.tableContentColumn.width -= 313;
        }
        else {
            $scope.tableContentColumn.width += 313;
        }
    };
    $scope.toggleBackLog = function () {
        // toggle for Sprint Backlog
        $scope.currentSprintBacklog = !$scope.currentSprintBacklog;
        if ($scope.currentSprintBacklog) {
            $scope.tableContentColumn.width -= 313;
        }
        else {
            $scope.tableContentColumn.width += 313;
        }
    };
    $scope.toggleInProgress = function () {
        // toogle for sprintInprogress
        $scope.inprogressSprint = !$scope.inprogressSprint;
        if ($scope.inprogressSprint) {
            $scope.tableContentColumn.width -= 313;
        }
        else {
            $scope.tableContentColumn.width += 313;
        }
    };
    $scope.toggleDone = function () {
        //toggle for done
        $scope.IsDone = !$scope.IsDone;
        if ($scope.IsDone) {
            $scope.tableContentColumn.width -= 313;
        }
        else {
            $scope.tableContentColumn.width += 313;
        }
    };
    $scope.toogleChart = function () {
        // toogle for chart
        $scope.isChart = !$scope.isChart;
        if ($scope.isChart) {
            $scope.tableContentColumn.width -= 313;
        }
        else {
            $scope.tableContentColumn.width += 313;
        }
    };
}
function convertToDate(date) {
    var newdate = date.getDate();
    var newMonth = date.getMonth() + 1;
    var newYear = date.getFullYear();
    var convertDate = new Date(newdate, newMonth, newYear);
}
function ArrageUserstory($scope) {
    try
    {
        if ($scope.productbacklogUserstory.length > -1) {
            var index = 0;
            $scope.userstoriesInCurrentBacklog = [];
            //$scope.InProgress
            //$scope.Done
            //$scope.BACKLOG
            //
            // DEFINE AN ARRAY USERSTORIES 

            for (var i = 0; i < $scope.productbacklogUserstory.length; i++) {
                if ($scope.productbacklogUserstory[i].sprintId > 0)
                {
                    for (var j = 0; j < $scope.sprints.length; j++)
                    {
                        // $scope.sprints[j] = [];
                        if ($scope.sprints[j].sprintId === $scope.productbacklogUserstory[i].sprintId)
                        {
                            $scope.sprints[j].userstories.push($scope.productbacklogUserstory[i]);
                        }
                    }
                }
                else {
                    $scope.userstoriesInCurrentBacklog[index] = $scope.productbacklogUserstory[i];
                    index++;
                }

            }
        }
        else {
            console.log($scope.userstoriesInCurrentBacklog);
        }

    } catch (ex) {

    }

    try {
        if ($scope.sprints.length > 0) {
            for (var i = 0; i < $scope.sprints.length; i++) {
                //console.log("aa");
                //console.log($scope.sprints.length);
                if ($scope.sprints[i].status === "BACKLOG")
                {
                    $scope.sprintBackLog.push($scope.sprints[i]);
                }
                else if ($scope.sprints[i].status === "DONE")
                {
                    $scope.sprintDone.push($scope.sprints[i]);
                }
                else {
                    $scope.sprintInProgress.push($scope.sprints[i]);
                }
            }
        }
        else {

        }
        $scope.userstoriesInCurrentBacklog.dataTypes = ["userstory"];
    } catch (ex) {

    }
    // console.log($scope.sprintInProgress);


}
/*
 * Calculate Point in one sprint
 */
function calculatePoint(array) {
    var point = {
        totalPoint: 0,
        completePoint: 0
    };
    for (var i = 0; i < array.length; i++)
    {
        point.totalPoint += parseInt(array[i].point);
        if (array[i].status === "DONE")
        {
            point.completePoint += parseInt(array[i].point);
        }
    }
    return point;
}
/*
 * 
 * @param {type} firstDate
 * @param {type} secondDate
 * @returns {Number of days}
 */
function getDatesBetweenTwoDates(firstDate, secondDate) {
    var oneday = 24 * 60 * 60 * 1000;
    //console.log(firstDate + ": " + secondDate);
    var FirstDate = new Date(firstDate);
    var SecondDate = new Date(secondDate);
    var diffDays = (Math.abs((FirstDate.getTime() - SecondDate.getTime()) / (oneday))) + 1;
    // console.log(diffDays + 1);
    return diffDays;
    //var dateArray = getDates(FirstDate,SecondDate);
    //console.log(dateArray);
}
/*
 * This function for get an array name between two dates.
 */
/*
 * Draw Burn Down Chart of course
 */
function DrawBurnDownChart($scope) {
    var data = {
        "yData": [{
                "name": "Planned",
                "data": []
            }, {
                "name": "Actual",
                "data": []
            }]};
    try {

        var numberofDay;
        var sprintName = "";
        if ($scope.sprintInProgress[0]) {
            numberofDay = getDatesBetweenTwoDates($scope.sprintInProgress[0].start_date, $scope.sprintInProgress[0].end_date);
            var totalStory = 0, i = 0;
            if ($scope.sprintInProgress[0].totalPoint) {
                totalStory = parseInt($scope.sprintInProgress[0].totalPoint);
                var divider = totalStory / numberofDay;
                //console.log(divider);
                // var divider = totalStory / numerOdDay;
                while (totalStory > 0)
                {
                    data.yData[0].data[i] = totalStory;
                    totalStory -= divider;
                    i++;
                }
            }
            sprintName = $scope.sprintInProgress[0].sprintName.toString() || "";
        }

        $scope.chartConfig = {
            options: {
                chart: {
                    type: 'line'
                },
                plotOptions: {
                    series: {
                        stacking: ''
                    }
                }
            },
            series: data.yData,
            title: {
                text: sprintName
            },
            credits: {
                enabled: true
            },
            loading: false,
            size: {}
        };
    } catch (ex) {
        console.log(ex);
    }
}
function getUserstoriesBySprintId(array, sprintId) {
    var USarray = [];
    if (array.length > 0) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].sprintId === sprintId)
            {
                USarray.push(array[i]);
            }
        }
    }
    return USarray;
}
//
function DetailCtrlWithUserId($scope, $http, $location, $routeParams, Data) {
    var project = {
        "projectId": $routeParams.id,
        "userId": $routeParams.userId
    };

    Data.getProjectByProjectID2('project', project).then(function (result) {


    });
}
function DefaultCtrl($scope, $http, $location, $routeParams, Data) {

}
// ARRAGE
function getProjectInProgressing(array) {
    var returnArray = Array();
    var j = 0;
    for (var i = 0; i < array.length; i++) {

        if (array[i].status === "IN PROGRESSING" && array[i].isArchive !== "1")
        {
            returnArray[j] = array[i];
            j++;
        }
    }
    return returnArray;
}
function getProjectPOSTPONE(array) {
    var returnArray = Array();
    var j = 0;
    for (var i = 0; i < array.length; i++) {
        if (array[i].status === "POST PONE" && array[i].isArchive !== "1")
        {
            returnArray[j] = array[i];
            j++;
        }
    }
    return returnArray;
}
function getProjectDONE(array) {
    var returnArray = Array();
    var j = 0;
    for (var i = 0; i < array.length; i++) {
        if (array[i].status === "DONE" && array[i].isArchive !== "1")
        {
            returnArray[j] = array[i];
            j++;
        }
    }
    return returnArray;
}
// FOR get user name cookie 
function deleteCookie(cname) {
    if (isSetCookie(cname)) {
        document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        $("#log-in-button").show();
        $(".log-in-Successfull").hide();
        // redirect
        window.location.replace("http://localhost:10000");
        window.location.href = "http://localhost:10000";
    }
}
function deleteAllCookies() {
    var c = document.cookie.split("; ");
    for (i in c)
        document.cookie = /^[^=]+/.exec(c[i])[0] + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0, num = ca.length; i < num; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1);
        if (c.indexOf(name) !== -1) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function isSetCookie(cname) {
    var vote = getCookie(cname);
    if (vote !== "") {
        //da set
        return true;
    } else {
        //chua set
        return false;
    }
}