app.service('shareMemberInProject', function(){
    var property = "first";
    var member = [];        
       return {
         getUserMemberAndRole: function() {
             return member;
         },
         setUserMemberAndRole: function(usermember) {
             member = usermember;
         }
       };
    
}).service('sharedUserId', function() {
    var userId = 0;
    return {
        getuserId: function() {
            return userId;
        },
        setuserId: function(userid) {
            userId = userid;
        }
    };
})
/// return true when Authorization
/// return false when Unauthorization
.service('checkRole', function(user,role){
    if(angular.equals(user.roleName, role)) {
        return true;
    }
    return false;
})
;