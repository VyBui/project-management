app.filter('filterTaskByNameEstimatePriority', function () {
    return function (tasks, tags) {
        if (tags.length === 0) {
            return tasks;
        }
        else {
            var filtered = [];
            if (tags.length >= 2) {
                /// And condition
                var index = tags.length;
                (tasks || []).forEach(function (task) {
                    var matches = true;
                    var match = function (task, tagName) {
                        var regex = new RegExp(tagName.text, 'i');
                        return task.taskName.toString().search(regex) === 0 ||
                                task.status.toString().search(regex) === 0 ||
                                task.estimateTime.toString().search(regex) === 0;
                    };

                    tags.forEach(function (tagName) {
                        matches = matches && match(task, tagName);
                    });
                    if (matches) {
                        filtered.push(task);
                    }
                });
            }
            else {
                // Or condition
                (tasks || []).forEach(function (task) {
                    //  console.log();
                    try {
                        var matches = tags.some(function (tagName) {
                            return (angular.lowercase(task.taskName).indexOf(angular.lowercase(tagName.text)) > -1) ||
                                    (angular.lowercase(task.estimateTime).indexOf(angular.lowercase(tagName.text)) > -1) ||
                                    (angular.lowercase(task.priority).indexOf(angular.lowercase(tagName.text)) > -1) ||
                                    (angular.lowercase(task.status).indexOf(angular.lowercase(tagName.text)) > -1);
                        });
                        if (matches) {
                            filtered.push(task);
                        }
                    } catch (ex) {
                        console.log(ex);
                    }

                });
            }
            return filtered;
        }
    };

})
        .filter('orderObjectByPosition', function () {
            return function (input, attribute) {
                if (!angular.isObject(input))
                    return input;

                var array = [];
                for (var objectKey in input) {
                    array.push(input[objectKey]);
                }

                array.sort(function (objecta, objectb) {
                    objecta = parseInt(objecta[attribute]);
                    objectb = parseInt(objectb[attribute]);
                    return objecta - objectb;
                });
                return array;

            };
        });