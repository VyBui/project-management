<?php

header("Access-Control-Allow-Origin: *");
require("../DAL.php");
$data = json_decode(file_get_contents("php://input"));

////// Count column

$myDataBase = new DataAccessLayer();
$getNumberOfColumn = $myDataBase->executeQuery("SELECT COUNT(*) as num FROM ScrumBoardColumn WHERE projectId='" . $data->projectId . "'");
$row = mysqli_fetch_array($getNumberOfColumn, MYSQLI_ASSOC);

if ($row['num'] > 0){
    $ordinaryPosition = $row['num'] + 1;
    $positionFornewColumn = $row['num'];
    /*
     UPDATE DONE COLUMN   
     *      */
    $updateDONEColumn = "UPDATE ScrumBoardColumn SET positionColumn = '" . $ordinaryPosition . "' WHERE projectId = '" . $data->projectId . "' AND name='DONE'";
    $isUpdateDONE = $myDataBase->executeQuery($updateDONEColumn);
    if ($isUpdateDONE) {
        $addColumnQuery = "INSERT INTO ScrumBoardColumn (name,projectId,positionColumn,Description,status ,isCanDelete) "
                . "VALUES ('" . $data->name . "','" . $data->projectId . "','" . $positionFornewColumn . "','" . $data->Description . "','" . $data->status . "','".$data->isCanDelete."')";

        $lastId = $myDataBase->returnLastId($addColumnQuery);
        if ($lastId) {
            $arr = array(
                "scrumBoardColumnId" => $lastId,
                "positionColumn" => $positionFornewColumn,
            );

            echo  $json_response = json_encode($arr);
        } else {
            echo "error";
        }
    }
} else {
    echo $row['num'];
}

/// update position for DONE


