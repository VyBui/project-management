<?php

header("Access-Control-Allow-Origin: *");
require("../DAL.php");
$data = json_decode(file_get_contents("php://input"));

$myDataBase = new DataAccessLayer();
$queryUpdateScrumBoardColumn = "UPDATE ScrumBoardColumn SET"
        . " name='" . $data->name . "',"
        . "positionColumn = '" . $data->positionColumn . "',"
        . "Description='" . $data->Description . "'"
        . ",isArchive='" . $data->isArchive . "'"
        . "where scrumBoardColumnId = '" . $data->scrumBoardColumnId . "'";

if ($myDataBase->executeNonQuery($queryUpdateScrumBoardColumn)) {
    if ($data->isArchive) {
        /// GEt all scrumboard Column
        // update for all Task
        /// make all task belong this column is archive
        $archiveTasksQuery = "UPDATE tasks SET"
                . " isArchive = '" . $data->isArchive . "'"
                . "where scrumBoardColumnId = '" . $data->scrumBoardColumnId . "'";
        $isOk = $myDataBase->executeNonQuery($archiveTasksQuery);
        if ($isOk) {
            $getScrumBoardColumn = " SELECT * FROM ScrumBoardColumn where projectId = '" . $data->projectId . "' and isArchive = 0";
            $getData = $myDataBase->executeQuery($getScrumBoardColumn);
            $arr = array();
            if ($getData->num_rows > 0) {
                while ($row = $getData->fetch_assoc()) {
                    $arr[] = $row;
                }
            } else if ($getData->num_rows == 0) {
                echo "Data not found";
            } else {
                echo json_encode("Error  '" . $data->userId . "'");
            }

            echo $json_response = json_encode($arr);
        }
        else {
            echo $archiveTasksQuery;
        }
    }
} else {
    echo "error";
}
