<?php

$_conn;
class DataAccessLayer {
    //load     
    public function _getConnection() {
        require("connectString.php");
        $_conn = mysqli_connect($_server, $_username, $_password, $_database);
        // check _connection
        if (mysqli_connect_errno()) {
            printf('Connect failed: %s\n', mysqli_connect_error());
            exit();
        }
        return $_conn;
    }
    

    //execute select command
    public function executeQuery($strSql) {
        try {
            $connection = $this->_getConnection();
            $runCmd = mysqli_query($connection, $strSql);
            mysqli_close($connection);
            return $runCmd;
        } catch (mysqli_sql_exception $ex) {
            showErr($ex);
        }
    }

    //execute others commands except for select one
    public function executeNonQuery($runCmd) {
        try {
            $connection = $this->_getConnection();
            $runCmd = mysqli_query($connection, $runCmd);
            mysqli_close($connection);
            if ($runCmd) {
                return true;
            } else {
                return false;
            }
        } catch (mysqli_sql_exception $ex) {
            showErr($ex);
        }
    }
    
    public function returnLastId($query) {
        try{
            $connection = $this->_getConnection();
            $query = mysqli_query($connection, $query);
             
            if($query) {
                 $last_id = mysqli_insert_id($connection);
                 mysqli_close($connection);
                 return $last_id;
            }
            else {
                mysqli_close($connection);
                return false;
            }
            }catch(mysqli_sql_exception $ex) {
            showErr($ex);
        }
    }

    //show the bug
    public function showErr(mysqli_sql_exception $ex) {
        print "Error Code <br>" . $ex->getCode();
        print "Error Message <br>" . $ex->getMessage();
        print "Strack Trace <br>" . nl2br($ex->getTraceAsString());
    }

}

?>