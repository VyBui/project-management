(function () {
    angular.module('visorSampleApp', ['visor', 'ngRoute', 'ngCookies'])
            .config(function (visorProvider, $routeProvider) {
                visorProvider.authenticate = function ($cookieStore, $q, $rootScope) {
                    var user = $cookieStore.get("user");
                    console.log(user);
                    if (user) {
                        $rootScope.user = user;
                        return $q.when(user);
                    } else {
                        console.log("when i was your man");
                        return $q.reject(null);
                    }
                };
                visorProvider.doOnNotAuthorized = function ($location, restrictedUrl) {
                    $location.url("access_denied?prevUrl=" + encodeURIComponent(restrictedUrl));
                };
                $routeProvider.when("/home", {
                    templateUrl: "app/home.html",
                    controller: function ($scope, visor) {
                        console.log("Home!!!");
                    }
                })
                        .when("/login", {
                            templateUrl: 'app/login.html',
                            controller: function ($scope, visor, $rootScope, $cookieStore) {
                                console.log("Login page");
                                $scope.login = function () {
                                    var user = {is_admin: $scope.is_admin};
                                    $cookieStore.put("user", user);
                                    console.log(user);
                                    $rootScope.user = user;
                                    console.log($rootScope);
                                    alert("Check Authenticated");
                                    visor.setAuthenticated(user);
                                };
                            },
                            restrict: function (user) {
                                return user === undefined;
                            }
                        })
                        .when("/private", {
                            templateUrl: "app/private.html",
                            controller: function($scope){
                                console.log("private page");
                                console.log($scope);
                            },
                            restrict: function (user) {
                                console.log(!!user);
                                return !!user;
                            }
                        })
                        .when("/access_denied", {
                            templateUrl: "app/access_denied.html",
                            controller: function ($scope, $routeParams) {
                                console.log("access Deined " +  $routeParams.prevUrl);
                                $scope.prevUrl = $routeParams.prevUrl;
                            }
                        })
                        .when("/admin", {
                            templateIr: "app/admin.html",
                            controller: function ($scope) {
                                console.log("admin page");
                            },
                            restrict: function (user) {
                                return user && user.is_admin;
                            }
                        })
                        .otherwise({redirectTo: "/home"});

            })
            .controller("MainCtrl", function ($scope, $cookieStore, $rootScope, $route, visor, $location) {
                $scope.$route = $route;
                $scope.logout = function () {

                    /// remove user
                    $cookieStore.remove("user");
                    $rootScope.user = undefined;
                    visor.setUnauthenticated();
                   //visor.set();
                    $location.url("/home");
                };
            });
})();

